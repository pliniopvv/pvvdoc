/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc.gui;

import br.com.pvv.pvvdoc.plugin.lra.Ponto;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author GanGss
 */
public class PontoEditarDescricaoController implements Initializable {

    private Ponto ponto;

    @FXML
    private TextArea textarea;
    @FXML
    private Button resetDescricao;

    public Ponto getPonto() {
        return ponto;
    }

    public void setPonto(Ponto ponto) {
        this.ponto = ponto;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.ponto = PontoEditarDescricaoStart.getPonto();
        textarea.setText(ponto.getDescricao());

        resetDescricao.setOnAction(e -> {
            ponto.setDescricao(null);
            Platform.runLater(() -> textarea.setText(ponto.getDescricao()));
        });
    }

    @FXML
    private void SalvarEFechar(ActionEvent event) {
        ponto.setDescricao(textarea.getText());
        PontoEditarDescricaoStart.getStage().close();
    }

}
