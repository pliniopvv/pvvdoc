package br.com.pvv.doc.gui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import br.com.pvv.doc.mem.RelatorioMemory;
import br.com.pvv.pvvdoc.plugin.lra.Leitura;
import br.com.pvv.pvvdoc.plugin.lra.NCA;
import br.com.pvv.pvvdoc.plugin.lra.Ponto;
import br.com.pvv.pvvdoc.plugin.lra.utils.DateAdapter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.layout.VBox;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author GanGss
 */
public class EditarLeiturasController implements Initializable {

    @FXML
    private TextField tx_numero;
//    @FXML
//    private TextField tx_dba;
//    @FXML
//    private ToggleButton tg_cod1;
//    @FXML
//    private ToggleGroup codigo;
//    @FXML
//    private ToggleButton tg_cod2;
//    @FXML
//    private ToggleButton tg_cod3;
//    @FXML
//    private ToggleButton tg_codr;
    @FXML
    private Button bt_fechar;
//    @FXML
//    private HBox box_Leituras;
    @FXML
    private RadioButton rd_nca;
    @FXML
    private RadioButton rd_codigos;
    @FXML
    private BarChart<String, Number> grafico;

    private Ponto ponto;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //configurações de inicialização básicas
        EditarLeiturasStart.setController(this);
        //
//        box_Leituras.getChildren().removeAll(box_Leituras.getChildren());
    }

    HashMap<Integer, VBox> control_leituras = new HashMap<>();
    ToggleGroup grupo_slider_leituras = new ToggleGroup();
    int cursor = -1;
    int selected[] = {-1, -1};
    ArrayList<Leitura> leituras_selecionadas = new ArrayList<>();

//    private VBox buildLeituraSlider(int index, Leitura l) {
//        VBox v = new VBox();
//
//        v.setPrefWidth(10.00);
//        v.setMaxWidth(VBox.USE_PREF_SIZE);
//        v.setMaxHeight(VBox.BASELINE_OFFSET_SAME_AS_HEIGHT);
//        v.alignmentProperty().set(Pos.CENTER);
//
//        control_leituras.put(index, v);
//
//        // colorindo hbox de acordo com as visões ;;
//        if (rd_codigos.isSelected()) {
//            if (l.getCodigo() == Leitura.CODIGO_1) {
//                v.setStyle("-fx-background-color: #955;");
//            } else if (l.getCodigo() == Leitura.CODIGO_2) {
//                v.setStyle("-fx-background-color: #559;");
//            } else if (l.getCodigo() == Leitura.CODIGO_3) {
//                v.setStyle("-fx-background-color: #595;");
//            } else {
//                v.setStyle("-fx-background-color: #999;");
//            }
//        }
//
//        // config RadioButton - seletor
//        RadioButton rb = new RadioButton();
//        rb.setToggleGroup(grupo_slider_leituras);
//        rb.setId(String.valueOf(index));
//
//        // config Slider ;;
//        Slider s = new Slider();
//        s.setMax(130.00);
//        s.setPrefHeight(220.00);
//        s.setMaxHeight(Slider.USE_PREF_SIZE);
//        s.setValue(l.getLAeq());
//        s.setId(String.valueOf(index));
//
//        s.setOrientation(Orientation.VERTICAL);
//
//        // config EVENTOS - rádiobutton seletor
//        s.setDisable(true);
//
//        s.setOnMouseReleased(e -> {
//            for (Leitura leitura : leituras_selecionadas) {
//                leitura.setLeq(s.getValue());
//            }
//        });
//
//        rb.setOnMouseClicked(e -> {
//            if ((cursor == -1)) {
//                cursor = Integer.parseInt(rb.getId());
//                selected[0] = cursor;
//            } else if ((cursor != Integer.parseInt(rb.getId())) && (selected[1] == -1)) {
//                selected[1] = Integer.parseInt(rb.getId());
//                Arrays.sort(selected);
//                cursor = selected[0];
//            } else {
//                selected[0] = -1;
//                selected[1] = -1;
//                cursor = -1;
//            }
//            reShowPontos();
//        });
//
//        v.getChildren().add(rb);
//        v.getChildren().add(s);
//
//        // config ATIVIDADE DO CURSOR.
//        if (cursor == index) {
//            SelectedSlider(s, l);
//            s.setDisable(false);
//            leituras_selecionadas.add(l);
//        } else if (((cursor != index) && (cursor != -1)) && (selected[0] < index) && (index <= selected[1])) {
//            SelectedSlider(s, l);
//            leituras_selecionadas.add(l);
//        }
//
//        return v;
//    }
//    private void enableControlPanel() {
//        tg_cod1.setDisable(false);
//        tg_cod2.setDisable(false);
//        tg_cod3.setDisable(false);
//        tg_codr.setDisable(false);
//    }
//    private void disableControlPanel() {
//        tg_cod1.setDisable(true);
//        tg_cod2.setDisable(true);
//        tg_cod3.setDisable(true);
//        tg_codr.setDisable(true);
//
//        tg_cod1.setSelected(false);
//        tg_cod2.setSelected(false);
//        tg_cod3.setSelected(false);
//        tg_codr.setSelected(false);
//
//        tx_dba.setText("");
//    }
    @FXML
    private void setDba(InputMethodEvent event) {
    }

//    @FXML
//    private void setCode(ActionEvent event) {
//        tg_cod1.setEffect(null);
//        tg_cod2.setEffect(null);
//        tg_cod3.setEffect(null);
//        tg_codr.setEffect(null);
//
//        if (tg_cod1.isSelected()) {
//            tg_cod1.setEffect(new ColorAdjust(0.00, 0.75, 0.75, 0.00));
//        } else if (tg_cod2.isSelected()) {
//            tg_cod2.setEffect(new ColorAdjust(0.00, 0.75, 0.75, 0.00));
//        } else if (tg_cod3.isSelected()) {
//            tg_cod3.setEffect(new ColorAdjust(0.00, 0.75, 0.75, 0.00));
//        } else {
//            tg_codr.setEffect(new ColorAdjust(0.00, 0.75, 0.75, 0.00));
//        }
//
//        //      VERIFICA SE É TUDO RUÍDO AMBIENTE;
//        if (ponto.getLeituras(Leitura.RESIDUAL).length == 0) {
//            ponto.setFullRuidoAmbiente(true);
//        } else {
//            ponto.setFullRuidoAmbiente(false);
//        }
//    }
    @FXML
    private void Fechar(ActionEvent event) {
        PontoEditorStart.getController().reconstruirTabelas();
        EditarLeiturasStart.getStage().close();
    }

    @FXML
    public void reShowPontos() {
        grafico.getData().clear();
        showPontos(this.ponto);
    }

    public void showPontos(Ponto p) {
        this.ponto = p;
        tx_numero.setText(String.valueOf(p.getNumero()));

        XYChart.Series<String, Number> sc = new XYChart.Series<>();
        for (Leitura leitura : p.getLeituras()) {

            XYChart.Data data = new XYChart.Data(DateAdapter.HorarioCalendar(leitura.getHorario()), leitura.getLAeq());
            data.nodeProperty().addListener(new ChangeListener<Node>() {

                @Override
                public void changed(ObservableValue<? extends Node> observable, Node oldValue, Node node) {
                    if (rd_codigos.isSelected()) {
                        switch (leitura.getCodigo()) {
                            case Leitura.CODIGO_1:
                                node.setStyle("-fx-bar-fill: red;");
                                break;
                            case Leitura.CODIGO_2:
                                node.setStyle("-fx-bar-fill: green;");
                                break;
                            case Leitura.CODIGO_3:
                                node.setStyle("-fx-bar-fill: blue;");
                                break;
                            default:
                                node.setStyle("-fx-bar-fill: black;");
                                break;
                        }
                    } else {
                        NCA nca = RelatorioMemory.getRelatorio().getNca();
                        double limit = nca.getLimite(ponto.getClassificacao());
                        if (leitura.getLAeq() <= limit) {
                            node.setStyle("-fx-bar-fill: black;");
                        } else {
                            node.setStyle("-fx-bar-fill: red;");
                        }
                    }
                }
            });
            sc.getData().add(data);
        }
        grafico.getData().add(sc);
    }

//    private void SelectedSlider(Slider s, Leitura l) {
//        control_leituras.get(Integer.parseInt(s.getId())).setStyle("-fx-background-color: #99d;");
//        ((RadioButton) control_leituras.get(Integer.parseInt(s.getId())).getChildren().get(0)).setSelected(true);
//        enableControlPanel();
//        tx_dba.setText(String.valueOf(l.getLAeq()));
//
//        switch (l.getCodigo()) {
//            case Leitura.CODIGO_1:
//                tg_cod1.setSelected(true);
//                break;
//            case Leitura.CODIGO_2:
//                tg_cod2.setSelected(true);
//                break;
//            case Leitura.CODIGO_3:
//                tg_cod3.setSelected(true);
//                break;
//        }
//
//        tg_cod1.setOnAction(ev -> {
//            setCode(ev);
//            if (tg_cod1.isSelected()) {
//                for (Leitura leitura : leituras_selecionadas) {
//                    leitura.setCodigo(Leitura.CODIGO_1);
//                }
//            }
//        });
//        tg_cod2.setOnAction(ev -> {
//            setCode(ev);
//            if (tg_cod2.isSelected()) {
//                for (Leitura leitura : leituras_selecionadas) {
//                    leitura.setCodigo(Leitura.CODIGO_2);
//                }
//            }
//        });
//        tg_cod3.setOnAction(ev -> {
//            setCode(ev);
//            if (tg_cod3.isSelected()) {
//                for (Leitura leitura : leituras_selecionadas) {
//                    leitura.setCodigo(Leitura.CODIGO_3);
//                }
//            }
//        });
//        tg_codr.setOnAction(ev -> {
//            setCode(ev);
//            if (tg_codr.isSelected()) {
//                for (Leitura leitura : leituras_selecionadas) {
//                    leitura.setCodigo(Leitura.RESIDUAL);
//                }
//            }
//        });
//    }
    @FXML
    public void setNumeroPonto() {
        try {
            int numero = Integer.parseInt(tx_numero.getText());
            ponto.setNumero(numero);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao converter número.", "ERRO.", JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
        }
    }
}
