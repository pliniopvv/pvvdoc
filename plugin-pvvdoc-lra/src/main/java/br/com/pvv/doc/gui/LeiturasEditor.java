/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc.gui;

import br.com.pvv.err.LogFX;
import br.com.pvv.pvvdoc.plugin.lra.Leitura;
import br.com.pvv.pvvdoc.plugin.lra.Ponto;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 *
 * @author GanGss
 */
public class LeiturasEditor extends Application {

    private static Stage stage = null;
    private static Ponto ponto = null;

    public LeiturasEditor() {
        setPonto(null);
    }

    public LeiturasEditor(Ponto ponto) {
        setPonto(ponto);
    }

    public static void setStage(Stage stage) {
        LeiturasEditor.stage = stage;
    }

    public static void setPonto(Ponto ponto) {
        LeiturasEditor.ponto = ponto;
    }

    public static Stage getStage() {
        return stage;
    }

    public static Ponto getPonto() {
        return ponto;
    }

    LeiturasEditor(ArrayList<Leitura> leituras) {
        ponto = new Ponto();
        ponto.setLeituras(leituras.toArray(new Leitura[leituras.size()]));
        ponto.setClassificacao(Ponto.MANHA);
        ponto.setNome("MANUAL");
        ponto.setNumero(1);
    }

    @Override
    public void start(Stage stage) {
        setStage(stage);
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/LeiturasEditor.fxml"));
            stage.getIcons().add(new Image(getClass().getResourceAsStream("/br/com/pvv/imgs/logo.png")));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            LogFX.alert(ex);
            Logger.getLogger(LeiturasEditor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    void myStart(Window window, Stage stage) {
        setStage(stage);
        try {
            Parent root = FXMLLoader.load(getClass().getResource("LeiturasEditor.fxml"));
            stage.getIcons().add(new Image(getClass().getResourceAsStream("/br/com/pvv/imgs/logo.png")));

            Scene scene = new Scene(root);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(window);
            stage.getIcons().add(new Image(getClass().getResourceAsStream("/br/com/pvv/imgs/logo.png")));
            stage.setScene(scene);
            stage.showAndWait();
        } catch (IOException ex) {
            LogFX.alert(ex);
            Logger.getLogger(LeiturasEditor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
