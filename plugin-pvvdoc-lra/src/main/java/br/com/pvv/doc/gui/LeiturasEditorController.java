/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc.gui;

import br.com.pvv.doc.gui.util.GraficoGui;
import br.com.pvv.err.LogFX;
import br.com.pvv.pvvdoc.plugin.lra.Leitura;
import br.com.pvv.pvvdoc.plugin.lra.Ponto;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author GanGss
 */
public class LeiturasEditorController implements Initializable {

    @FXML
    private ImageView imagem;
    @FXML
    private TextField tf_numero;
    @FXML
    private TextField tf_periodo;
    // FERRAMENTAS
    private Ponto ponto = null;
    private GraficoGui pintor = null;
    // ELEMENTOS
    private int pcursor = -1;
    private int scursor = -1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Ponto p = LeiturasEditor.getPonto();
        ponto = new Ponto();
        ponto.setNumero(p.getNumero());
        ponto.setClassificacao(p.getClassificacao());

        ArrayList<Leitura> nl = new ArrayList<Leitura>();
        for (Leitura l : p.getLeituras()) {
            Leitura ll = new Leitura();
            ll.setCodigo(l.getCodigo());
            ll.setDuracao(l.getDuracao());
            ll.setHorario(l.getHorario());
            ll.setLAeq(l.getLAeq());
            ll.setLmax(l.getLmax());
            ll.setLmin(l.getLmin());

            nl.add(ll);
        }
        ponto.setLeituras(nl.toArray(new Leitura[nl.size()]));

        if (ponto == null) {
            JOptionPane.showMessageDialog(null, "Ainda não implementado criação de pontos.", "ERRO!", JOptionPane.ERROR_MESSAGE);
            LeiturasEditor.getStage().close();
            return;
        }

        tf_numero.setText(String.valueOf(ponto.getNumero()));
        tf_periodo.setText(Ponto.ClassificacaoToString(ponto.getClassificacao()));
        pintor = new GraficoGui(ponto);
        pintarGrafico();
    }

    @FXML
    private void onResidual(ActionEvent event) {
//        System.out.println(pcursor);
//        System.out.println((scursor - pcursor));
        if (scursor == -1) {
            return;
        }

        Leitura[] leituras = ponto.getLeituras();
        int leiturasSeguidas = scursor - pcursor;

        for (int i = pcursor; i < pcursor + leiturasSeguidas; i++) {
            leituras[i].setCodigo(Leitura.RESIDUAL);
        }
        pintarGrafico();
    }

    @FXML
    private void onCod1(ActionEvent event) {
        if (scursor == -1) {
            return;
        }

        Leitura[] leituras = ponto.getLeituras();
        int leiturasSeguidas = scursor - pcursor;

        for (int i = pcursor; i < pcursor + leiturasSeguidas; i++) {
            leituras[i].setCodigo(Leitura.CODIGO_1);
        }
        pintarGrafico();
    }

    @FXML
    private void onCod2(ActionEvent event) {
        if (scursor == -1) {
            return;
        }

        Leitura[] leituras = ponto.getLeituras();
        int leiturasSeguidas = scursor - pcursor;

        for (int i = pcursor; i < pcursor + leiturasSeguidas; i++) {
            leituras[i].setCodigo(Leitura.CODIGO_2);
        }
        pintarGrafico();
    }

    @FXML
    private void onCod3(ActionEvent event) {
        if (scursor == -1) {
            return;
        }

        Leitura[] leituras = ponto.getLeituras();
        int leiturasSeguidas = scursor - pcursor;

        for (int i = pcursor; i < pcursor + leiturasSeguidas; i++) {
            leituras[i].setCodigo(Leitura.CODIGO_3);
        }
        pintarGrafico();
    }

    @FXML
    private void onSalvar(ActionEvent event) {
        Ponto p = LeiturasEditor.getPonto();
        p.setNumero(Integer.valueOf(tf_numero.getText()));
        p.setLeituras(ponto.getLeituras());
        try {
            PontoEditorStart.getController().reconstruirTabelas();
        } catch (Exception e) {
        }
        LeiturasEditor.getStage().close();
    }

    @FXML
    private void onFechar(ActionEvent event) {
        LeiturasEditor.getStage().close();
    }

    private void pintarGrafico() {
        try {
            // CONSTRUÇÃO DO GRÁFICO
            BufferedImage bi = pintor.getGraphicColored();
            Graphics2D g = (Graphics2D) bi.getGraphics();

            if (pcursor != -1) {
                g.setColor(Color.YELLOW);
                g.drawLine(
                        //                        pcursor + 50,
                        (int) (pcursor * pintor.getTamanhoLeitura() + 50),
                        0,
                        //                        pcursor + 50,
                        (int) (pcursor * pintor.getTamanhoLeitura() + 50),
                        325);
            }
            if (scursor != -1) {
                g.setColor(Color.YELLOW);
                g.drawLine(
                        //                        scursor + 50,
                        (int) (scursor * pintor.getTamanhoLeitura() + 50),
                        0,
                        //                        scursor + 50,
                        (int) (scursor * pintor.getTamanhoLeitura() + 50),
                        325);
            }
            g.dispose();

            // MANIPULAÇÃO PARA EXIBIÇÃO DO GRÁFICO.
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bi, "PNG", baos);
            baos.flush();

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            Platform.runLater(() -> {
                imagem.setImage(new Image(bais));
            });
        } catch (IOException ex) {
            LogFX.alert(ex);
            Logger.getLogger(LeiturasEditorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onGraficoClick(MouseEvent event) {
        if (event.getX() < 50.0) {
            return;
        }

        if (pcursor == -1) {
//            pcursor = (int) event.getX() - 50;
            pcursor = (int) ((event.getX() - 50) / pintor.getTamanhoLeitura() - 1);
        } else if (scursor == -1) {
//            scursor = (int) event.getX() - 50;
            scursor = (int) ((event.getX() - 50) / pintor.getTamanhoLeitura() + 1);
        } else {
            pcursor = -1;
            scursor = -1;
        }

        if ((pcursor != -1) && (scursor != -1) && (pcursor > scursor)) {
            int trade = pcursor;
            pcursor = scursor;
            scursor = trade;
        }
        pintarGrafico();
    }

}
