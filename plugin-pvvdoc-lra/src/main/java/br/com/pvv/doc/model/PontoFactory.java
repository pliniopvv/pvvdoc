/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc.model;

import br.com.pvv.pvvdoc.plugin.lra.Ponto;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author Plinio
 */
public class PontoFactory {

    private File dir;
    private Ponto ponto;

    public PontoFactory(File dir, Ponto ponto) {
        this.ponto = ponto;
        this.dir = dir;
    }

    public PontoFactory() {
    }

    public void save() throws IOException {
        File arquivo = new File(dir, "PONTO_" + Ponto.ClassificacaoToStringAbrev(ponto.getClassificacao()) + ponto.getNumero() + ".ptn");
        ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(
                        arquivo
                )
        );
        oos.writeObject(ponto);
        oos.close();
    }

    public PontoFactory load(File arquivo) throws IOException, ClassNotFoundException {
        this.dir = arquivo.getParentFile();
        ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(arquivo)
        );
        this.ponto = (Ponto) ois.readObject();
        ois.close();
        return this;
    }

    public Ponto getPonto() {
        return this.ponto;
    }
}
