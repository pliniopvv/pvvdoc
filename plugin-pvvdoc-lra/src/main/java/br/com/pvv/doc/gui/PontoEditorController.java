/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc.gui;

import br.com.pvv.doc.lra.ExecutablePlugin;
import br.com.pvv.doc.mem.RelatorioMemory;
import br.com.pvv.err.LogFX;
import br.com.pvv.pvvdoc.plugin.lra.DbTraitAdapter;
import br.com.pvv.pvvdoc.plugin.lra.Ponto;
import br.com.pvv.pvvdoc.plugin.lra.PontosManager;
import br.com.pvv.pvvdoc.plugin.lra.Relatorio;
import br.com.pvv.pvvdoc.plugin.lra.utils.PontoComparatorNameFile;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author GanGss
 */
public class PontoEditorController implements Initializable {

//    @FXML
//    private Button btn_sobe;
//    @FXML
//    private Button btn_desce;
    @FXML
    private Button btn_manha;
    @FXML
    private Button btn_tarde;
    @FXML
    private Button btn_noite;
    @FXML
    private Button btn_renumerar;
    @FXML
    private Button btn_editPonto;
    @FXML
    private Button btn_editarDescricao;
    @FXML
    private Button btn_org_nome;
    @FXML
    private Button btn_org_numero;
    @FXML
    private TableView<Ponto> table_manha;
    @FXML
    private TableColumn<Ponto, String> tc_m_numero;
    @FXML
    private TableColumn<Ponto, String> tc_m_nome;
    @FXML
    private TableColumn<Ponto, String> tc_m_LAeqG;
    @FXML
    private TableColumn<Ponto, String> tc_m_LAeqT;
    @FXML
    private TableColumn<Ponto, String> tc_m_Lra;
    @FXML
    private TableColumn<Ponto, String> tc_m_Lmin;
    @FXML
    private TableColumn<Ponto, String> tc_m_Lmax;
    @FXML
    private TableColumn<Ponto, String> tc_m_Lc;
    @FXML
    private TableView<Ponto> table_tarde;
    @FXML
    private TableColumn<Ponto, String> tc_t_numero;
    @FXML
    private TableColumn<Ponto, String> tc_t_nome;
    @FXML
    private TableColumn<Ponto, String> tc_t_LAeqG;
    @FXML
    private TableColumn<Ponto, String> tc_t_LAeqT;
    @FXML
    private TableColumn<Ponto, String> tc_t_Lra;
    @FXML
    private TableColumn<Ponto, String> tc_t_Lmin;
    @FXML
    private TableColumn<Ponto, String> tc_t_Lmax;
    @FXML
    private TableColumn<Ponto, String> tc_t_Lc;
    @FXML
    private TableView<Ponto> table_noite;
    @FXML
    private TableColumn<Ponto, String> tc_n_numero;
    @FXML
    private TableColumn<Ponto, String> tc_n_nome;
    @FXML
    private TableColumn<Ponto, String> tc_n_LAeqG;
    @FXML
    private TableColumn<Ponto, String> tc_n_LAeqT;
    @FXML
    private TableColumn<Ponto, String> tc_n_Lra;
    @FXML
    private TableColumn<Ponto, String> tc_n_Lmin;
    @FXML
    private TableColumn<Ponto, String> tc_n_Lmax;
    @FXML
    private TableColumn<Ponto, String> tc_n_Lc;
    @FXML
    private ProgressIndicator progressIndicator;

    //
    //
    //
    private PontosManager pontosManager;

    ObservableList<Ponto> oManha = FXCollections.observableArrayList();
    ObservableList<Ponto> oTarde = FXCollections.observableArrayList();
    ObservableList<Ponto> oNoite = FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        PontoEditorStart.setController(this);

        tc_m_LAeqG.setCellValueFactory(new PropertyValueFactory<Ponto, String>("GlobalLAeq"));
        tc_m_LAeqT.setCellValueFactory(new PropertyValueFactory<Ponto, String>("LAeqTratado"));
        tc_m_Lc.setCellValueFactory(new PropertyValueFactory<Ponto, String>("LAeq"));
        tc_m_Lmax.setCellValueFactory(new PropertyValueFactory<Ponto, String>("LAeqMax"));
        tc_m_Lmin.setCellValueFactory(new PropertyValueFactory<Ponto, String>("LAeqMin"));
        tc_m_Lra.setCellValueFactory(new PropertyValueFactory<Ponto, String>("Lra"));
        tc_m_nome.setCellValueFactory(new PropertyValueFactory<Ponto, String>("Nome"));
        tc_m_numero.setCellValueFactory(new PropertyValueFactory<Ponto, String>("Numero"));

        tc_t_LAeqG.setCellValueFactory(new PropertyValueFactory<Ponto, String>("GlobalLAeq"));
        tc_t_LAeqT.setCellValueFactory(new PropertyValueFactory<Ponto, String>("LAeqTratado"));
        tc_t_Lc.setCellValueFactory(new PropertyValueFactory<Ponto, String>("LAeq"));
        tc_t_Lmax.setCellValueFactory(new PropertyValueFactory<Ponto, String>("LAeqMax"));
        tc_t_Lmin.setCellValueFactory(new PropertyValueFactory<Ponto, String>("LAeqMin"));
        tc_t_Lra.setCellValueFactory(new PropertyValueFactory<Ponto, String>("Lra"));
        tc_t_nome.setCellValueFactory(new PropertyValueFactory<Ponto, String>("Nome"));
        tc_t_numero.setCellValueFactory(new PropertyValueFactory<Ponto, String>("Numero"));

        tc_n_LAeqG.setCellValueFactory(new PropertyValueFactory<Ponto, String>("GlobalLAeq"));
        tc_n_LAeqT.setCellValueFactory(new PropertyValueFactory<Ponto, String>("LAeqTratado"));
        tc_n_Lc.setCellValueFactory(new PropertyValueFactory<Ponto, String>("LAeq"));
        tc_n_Lmax.setCellValueFactory(new PropertyValueFactory<Ponto, String>("LAeqMax"));
        tc_n_Lmin.setCellValueFactory(new PropertyValueFactory<Ponto, String>("LAeqMin"));
        tc_n_Lra.setCellValueFactory(new PropertyValueFactory<Ponto, String>("Lra"));
        tc_n_nome.setCellValueFactory(new PropertyValueFactory<Ponto, String>("Nome"));
        tc_n_numero.setCellValueFactory(new PropertyValueFactory<Ponto, String>("Numero"));

        table_manha.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table_tarde.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table_noite.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        table_manha.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                Ponto p = oManha.get(table_manha.getSelectionModel().getSelectedIndex());
                editarPonto(p);
            }
        });

        table_tarde.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                Ponto p = oTarde.get(table_tarde.getSelectionModel().getSelectedIndex());
                editarPonto(p);
            }
        });

        table_noite.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                Ponto p = oNoite.get(table_noite.getSelectionModel().getSelectedIndex());
                editarPonto(p);
            }
        });

        btn_editarDescricao.setOnAction(e -> {
            PontoEditarDescricaoStart.setPonto(selecteds.get(0));
            Stage stage = new Stage();
            stage.setTitle(selecteds.get(0).getNome() + " - Descrição");
            new PontoEditarDescricaoStart().start(stage);
        });

        Relatorio relatorio = RelatorioMemory.getRelatorio();
        if (relatorio == null) {
            RelatorioMemory.setRelatorio(new Relatorio());
            relatorio = RelatorioMemory.getRelatorio();
        }
        pontosManager = relatorio.getPontosManeger();
        if (pontosManager != null) {
            setPontosManeger(pontosManager);
        }
    }

    public void setPontosManeger(PontosManager pm) {
        this.pontosManager = pm;

        table_manha.setItems(oManha);
        table_tarde.setItems(oTarde);
        table_noite.setItems(oNoite);

        construirTabelas();
    }

    public PontosManager getPontosManeger() {
        return pontosManager;
    }

    public void reconstruirTabelas() {
        construirTabelas();
    }

    private void construirTabelas() {
        Ponto[] manha = pontosManager.getPontos(Ponto.MANHA);
        Ponto[] tarde = pontosManager.getPontos(Ponto.TARDE);
        Ponto[] noite = pontosManager.getPontos(Ponto.NOITE);

        oManha.clear();
        oTarde.clear();
        oNoite.clear();

        oManha.addAll(manha);
        oTarde.addAll(tarde);
        oNoite.addAll(noite);
    }

    @FXML
    public void renumerar() {
        if (oManha.size() > 0) {
            int numero = 0;
            for (Ponto ponto : oManha) {
                numero++;
                ponto.setNumero(numero);
            }
        }
        if (oTarde.size() > 0) {
            int numero = 0;
            for (Ponto ponto : oTarde) {
                numero++;
                ponto.setNumero(numero);
            }
        }
        if (oNoite.size() > 0) {
            int numero = 0;
            for (Ponto ponto : oNoite) {
                numero++;
                ponto.setNumero(numero);
            }
        }

        construirTabelas();
    }

    @FXML
    public void moverManha() {
        mover(selecteds, Ponto.MANHA);
    }

    @FXML
    public void moverTarde() {
        mover(selecteds, Ponto.TARDE);
    }

    @FXML
    public void moverNoite() {
        mover(selecteds, Ponto.NOITE);
    }
    ObservableList<Ponto> selecteds = null;

    public void mover(ObservableList<Ponto> obs, int destino_class) {
        if (obs.size() > 0) {
            for (int i = 0; i < obs.size(); i++) {
                obs.get(i).setClassificacao(destino_class);
            }
        }
        reconstruirTabelas();
    }

    private String[] decodPeriodo = {"MANHA", "TARDE", "NOITE"};
    private int codPeriodoSelected = 0;

    @FXML
    public void selectPontos() {
        if (table_manha.isFocused()) {
            codPeriodoSelected = 0;
            selecteds = table_manha.getSelectionModel().getSelectedItems();
        } else if (table_tarde.isFocused()) {
            codPeriodoSelected = 1;
            selecteds = table_tarde.getSelectionModel().getSelectedItems();
        } else { // table_noite.isFocuded() = true;
            codPeriodoSelected = 2;
            selecteds = table_noite.getSelectionModel().getSelectedItems();
        }
    }

    @FXML
    public void organizarPorNome() {
        PontoComparatorNameFile comparador = new PontoComparatorNameFile();

        Ponto[] pontos = pontosManager.getPontos();
        Arrays.sort(pontos, comparador);
        pontosManager.setPontos(pontos);

        reconstruirTabelas();
    }

    @FXML
    public void adicionarPonto(ActionEvent event) {
//        new NovasLeituras().modalStart(((Node) event.getSource()).getScene().getWindow(), new Stage());
//        reconstruirTabelas();
    }

    public final static int ORDEM_OBTER_LISTA_CATALOGOS = 100;
    public final static int ORDEM_OBTER_CATALOGO_COMPLETO = 101;

    @FXML
    public void sincronizarPvvLraMobile(ActionEvent event) {
//        String ip = JOptionPane.showInputDialog(null, "Digite o endereço IP do Aparelho:", "DÚVIDA?", JOptionPane.QUESTION_MESSAGE);
//        if ((ip == null)) {
//            return;
//        }
//        int PORT = 9001;
//
//        new Thread(() -> {
//            try {
//                Socket socket = new Socket(ip, PORT);
////                ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
////                ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
//                ConnectionOrderFactory order = new ConnectionOrderFactory(socket);
//                List<String> listaCatalogos = order.getCatalogos();
//                Platform.runLater(() -> {
//                    try {
//                        String catalogo = new ListSelector("Selecione o catálogo abaixo:", listaCatalogos).getSelectedString();
//                        List<Ponto> listaPontos = order.getPontos(catalogo);
//                        /*
//                        ADICIONANDO PONTOS À VIEW E AO RELATÓRIO;
//                         */
//                        PontosManager pm = new PontosManager();
//                        if (RelatorioMemory.getRelatorio().getPontosManeger() != null) {
//                            listaPontos.addAll(Arrays.asList(RelatorioMemory.getRelatorio().getPontosManeger().getPontos()));
//                        }
//                        pm.setPontos(listaPontos.toArray(new Ponto[listaPontos.size()]));
//                        setPontosManeger(pm);
//                        RelatorioMemory.getRelatorio().setPontosManeger(getPontosManeger());
//                    } catch (Exception e) {
//                        LogFX.alert(e);
//                    }
//
//                });
//            } catch (Exception e) {
//                LogFX.alert(e);
//                e.printStackTrace();
//            }
//        }).start();
        JOptionPane.showMessageDialog(null, "Desabilitado", "Desabilitado", JOptionPane.ERROR_MESSAGE);
    }

    @FXML
    public void removerPontos() {
        int i = JOptionPane.showConfirmDialog(null, "Tem certeza que deseja elimitar " + selecteds.size() + " pontos do período da " + decodPeriodo[codPeriodoSelected], "DUVIDA?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (i == 0) {
            List<Ponto> pontos = Arrays.asList(getPontosManeger().getPontos());
            ArrayList<Ponto> lista = new ArrayList<Ponto>(pontos);
            lista.removeAll(selecteds);
            getPontosManeger().setPontos(lista.toArray(new Ponto[lista.size()]));
            reconstruirTabelas();
        }
    }

    @FXML
    public void organizarPorNumero() {
        if (oManha.size() > 0) {
            Collections.sort(oManha);
        }
        if (oTarde.size() > 0) {
            Collections.sort(oTarde);
        }
        if (oNoite.size() > 0) {
            Collections.sort(oNoite);
        }
    }
//
//    public class Exportar extends Task {
//
//        public Exportar() {
//            table_manha.setEditable(false);
//            table_tarde.setEditable(false);
//            table_noite.setEditable(false);
//            progressIndicator.setVisible(true);
//        }
//
//        @Override
//        protected Object call() throws Exception {
//            DirectoryChooser dc = new DirectoryChooser();
//            dc.setTitle("Selecione o local onde salvar a pasta 'Pontos'.");
//            File outputDir = dc.showDialog(null);
//
//            if (outputDir == null) {
//                return null;
//            }
//
//            TexWritter t = new TexWritter(outputDir);
//
//            TexSessionPontos pt_manha = new TexSessionPontos(pm.getPontos(Ponto.MANHA));
//            TexSessionPontos pt_tarde = new TexSessionPontos(pm.getPontos(Ponto.TARDE));
//            TexSessionPontos pt_noite = new TexSessionPontos(pm.getPontos(Ponto.NOITE));
//
//            if (pm.hasPontos(Ponto.MANHA)) {
//                t.writeFile("\\temp\\pontos\\begin_manha.tex", pt_manha.getStartPageSessionPontos());
//                t.writeFile("\\temp\\pontos\\pontos_manha.tex", pt_manha.getPageSessionPontos());
//            }
//            if (pm.hasPontos(Ponto.TARDE)) {
//                t.writeFile("\\temp\\pontos\\begin_tarde.tex", pt_tarde.getStartPageSessionPontos());
//                t.writeFile("\\temp\\pontos\\pontos_tarde.tex", pt_tarde.getPageSessionPontos());
//            }
//            if (pm.hasPontos(Ponto.NOITE)) {
//                t.writeFile("\\temp\\pontos\\begin_noite.tex", pt_noite.getStartPageSessionPontos());
//                t.writeFile("\\temp\\pontos\\pontos_noite.tex", pt_noite.getPageSessionPontos());
//            }
//
//            //                                          
//            //                                          
//            //                          PONTOS.TEX FILE
//            //                                          
//            //                                          
//            //                                          
//            StringBuffer pontos_tex = new StringBuffer();
//            if (pm.hasPontos(Ponto.MANHA)) {
//                pontos_tex.append(""
//                        + "\\chapter{DESCRIÇÃO DOS RESULTADOS DO PERÍODO DA MANHÃ}\n"
//                        + "\n"
//                        + "\\input{temp/pontos/begin_manha.tex}\n"
//                        + "\n"
//                        + "\\input{temp/pontos/pontos_manha.tex}\n"
//                        + "\n"
//                        + "\\newpage \n"
//                        + "");
//            }
//            if (pm.hasPontos(Ponto.TARDE)) {
//                pontos_tex.append(""
//                        + "\\chapter{DESCRIÇÃO DOS RESULTADOS DO PERÍODO DA TARDE}\n"
//                        + "\n"
//                        + "\\input{temp/pontos/begin_tarde.tex}\n"
//                        + "\n"
//                        + "\\input{temp/pontos/pontos_tarde.tex}\n"
//                        + "\n"
//                        + "\\newpage \n"
//                        + "");
//            }
//            if (pm.hasPontos(Ponto.NOITE)) {
//                pontos_tex.append(""
//                        + "\\chapter{DESCRIÇÃO DOS RESULTADOS DO PERÍODO DA NOITE}\n"
//                        + "\n"
//                        + "\\input{temp/pontos/begin_noite.tex}\n"
//                        + "\n"
//                        + "\\input{temp/pontos/pontos_noite.tex}\n"
//                        + "\n"
//                        + "\\newpage \n"
//                        + "");
//            }
//            t.writeFile("\\temp\\PONTOS.tex", pontos_tex.toString());
//
//            //                                          
//            //                                          
//            //                          TABLES FILE
//            //                                          
//            //                                          
//            //                                   
//            TexTabela tt = new TexTabela(pm);
//            t.writeFile("\\temp\\tabela\\TabelaResumo.tex", tt.getIndexedTableTex());
//            t.writeFile("\\temp\\tabela\\repTabelaResumo.tex", tt.getTableTex());
//
//            return true;
//        }
//
//        @Override
//        protected void succeeded() {
//            progressIndicator.setVisible(false);
//            table_manha.setEditable(true);
//            table_tarde.setEditable(true);
//            table_noite.setEditable(true);
//
//            JOptionPane.showMessageDialog(null, "Arquivos elaborados com sucesso.", "Sucesso.", JOptionPane.INFORMATION_MESSAGE);
//        }
//
//    }
//

    @FXML
    public void abrirDbTrait(ActionEvent e) {

        try {
            DirectoryChooser dc = new DirectoryChooser();
            dc.setTitle("Selecione a pasta criada pelo Script do DbTrait.");
            File f = dc.showDialog(PontoEditorStart.getStage());

            if (f == null) {
                return;
            }

            if (pontosManager == null) {
                pontosManager = new DbTraitAdapter().parseDir(f);
            }

            RelatorioMemory.getRelatorio().setPontosManeger(pontosManager);
            setPontosManeger(pontosManager);
//            reconstruirTabelas();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Falha em carregar os arquivos: \nMensagem de erro:" + ex.getMessage(), "ERRO!", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(PontoEditorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void onExecutar(ActionEvent _e) {
        PontoEditorStart.getStage().close();
        Thread t = new Thread(() -> {
            for (ExecutablePlugin ep : PontoEditorStart.getObservablePlugin()) {
                try {
                    ep.execute();
                } catch (Exception e) {
                    LogFX.alert(e);
                }
            }
        });
        t.setDaemon(true);
        t.setPriority(Thread.MAX_PRIORITY);
        t.start();
    }
//    @FXML
//    public void exportar() {
////        Platform.runLater(new Exportar());
//    }

    @FXML
    public void editarPonto() {
        if (selecteds.size() <= 0) {
            return;
        }

        editarPonto(selecteds.get(0));
    }

    public void editarPonto(Ponto p) {
        Platform.runLater(() -> new LeiturasEditor(p).start(new Stage()));
//        EditarLeiturasStart els = new EditarLeiturasStart();
//        Stage s = new Stage();
//        s.setTitle(p.getNome() + " - Leituras");
//        els.start(s);
//        EditarLeiturasStart.getController().showPontos(p);
    }

    public void subirPonto() {
        if (selecteds.get(0).getNumero() == 1) {
            return;
        }

        Ponto oldP = null;
        for (Ponto ponto : pontosManager.getPontos(selecteds.get(0).getClassificacao())) {
            if (ponto.getNumero() == selecteds.get(0).getNumero() - 1) {
                oldP = ponto;
            }
        } // pega o ponto imediatamente acima;

        int i = 0;
        for (Ponto ponto : selecteds) {
            i++;
            int oldN = ponto.getNumero();
            ponto.setNumero(oldN - 1);
        } // reorganiza os números 

        oldP.setNumero(oldP.getNumero() + i);
        Arrays.sort(pontosManager.getPontos());
        reconstruirTabelas();
    }

    public void descerPonto() {

        int maxp = pontosManager.getPontos(selecteds.get(0).getClassificacao()).length;
        if (selecteds.get(0).getNumero() == maxp) {
            return;
        }

        Ponto nexP = null;
        for (Ponto ponto : pontosManager.getPontos(selecteds.get(0).getClassificacao())) {
            if (ponto.getNumero() == selecteds.get(selecteds.size() - 1).getNumero() + 1) {
                nexP = ponto;
            }
        }

        int i = 0;
        for (Ponto ponto : selecteds) {
            i--;
            int oldN = ponto.getNumero();
            ponto.setNumero(oldN + 1);
        } // reorganiza os números 

        nexP.setNumero(nexP.getNumero() + i);
        Arrays.sort(pontosManager.getPontos());
        reconstruirTabelas();
    }

}
