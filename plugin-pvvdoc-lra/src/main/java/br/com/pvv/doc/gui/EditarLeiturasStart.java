package br.com.pvv.doc.gui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import br.com.pvv.err.LogFX;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author GanGss
 */
public class EditarLeiturasStart extends Application {

    private static Stage stage;
    private static EditarLeiturasController controller;

    @Override
    public void start(Stage stage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/EditarLeituras.fxml"));
            Scene scene = new Scene(root);
            stage.getIcons().add(new Image(getClass().getResourceAsStream("/br/com/pvv/imgs/logo.png")));
            stage.setScene(scene);
            stage.show();

            this.stage = stage;
        } catch (IOException ex) {
            LogFX.alert(ex);
            Logger.getLogger(EditarLeiturasStart.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Stage getStage() {
        return EditarLeiturasStart.stage;
    }

    public static void setController(EditarLeiturasController controller) {
        EditarLeiturasStart.controller = controller;
    }

    public static EditarLeiturasController getController() {
        return controller;
    }

    public static void main(String[] args) {
        launch(args);
    }

}
