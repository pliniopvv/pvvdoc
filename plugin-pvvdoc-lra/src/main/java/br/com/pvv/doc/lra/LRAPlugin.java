/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc.lra;

import br.com.pvv.doc.PaginaPontoBuilder;
import br.com.pvv.doc.VariaveisScanner;
import br.com.pvv.pvvdoc.plugin.lra.Ponto;
import br.com.pvv.pvvdoc.plugin.lra.utils.DateAdapter;
import br.com.pvv.pvvdoc.plugin.lra.utils.Grafico;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javax.imageio.ImageIO;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

/**
 *
 * @author Plinio
 */
public class LRAPlugin implements ExecutablePlugin {

    private XWPFDocument documento;
    private List<Ponto> listaPontos;

    public LRAPlugin(XWPFDocument documento, List<Ponto> listaPontos) {
        this.documento = documento;
        this.listaPontos = listaPontos;
    }

    @Override
    public void execute() throws Exception {
        VariaveisScanner vs = new VariaveisScanner(documento);
//        List<String> listaVariaveisPlugins = vs.collect().stream().filter(v -> v.contains("PLUGIN")).collect(Collectors.toList());
        /*
        INCLUSaO DOS PONTOS
         */
        String[] varPeriodo = {"PLUGIN:TABELA-RESULTADOS-MANHA", "PLUGIN:TABELA-RESULTADOS-TARDE", "PLUGIN:TABELA-RESULTADOS-NOITE"};
        //ALTERNA OS PERiODOS
        for (int periodo = 0; periodo < 3; periodo++) {
            int speriodo = periodo;
            List<Ponto> listapontosDoPeriodo = listaPontos.stream().filter(p -> p.getClassificacao() == speriodo).sorted(Comparator.comparing(p -> p.getNumero())).collect(Collectors.toList());
            String var = varPeriodo[periodo];
            XWPFParagraph paragrafo = vs.getParagrafo(var);
            for (Ponto ponto : listapontosDoPeriodo) {
                buildTable(paragrafo, ponto);
            }
        }
        /*
        INCLUSaO DAS FOTOS
         */
 /*
        DASDOS GPS
         */

    }
    PaginaPontoBuilder.PERIODO[] ppbPeriodo = {PaginaPontoBuilder.PERIODO.MANHA, PaginaPontoBuilder.PERIODO.TARDE, PaginaPontoBuilder.PERIODO.NOITE};

    public void buildTable(XWPFParagraph p, Ponto ponto) throws IOException, InvalidFormatException {
        PaginaPontoBuilder pp = new PaginaPontoBuilder(p);

        pp.addTitle(ppbPeriodo[ponto.getClassificacao()])
                .addTableInfo(ponto.getNome(),
                        DateAdapter.CalendarReduzido(ponto.getInicio()),
                        DateAdapter.CalendarReduzido(ponto.getFim()),
                        ponto.getLAeqArredondado(),
                        ponto.getLAeqMin(),
                        ponto.getLAeqMax(),
                        ponto.getLAeqTratado(),
                        ponto.getLAeqTratado())
                .addDescription(ponto.getDescricao());

        Grafico g = new Grafico(ponto);

        if ((ponto.isFullResidual()) || (ponto.isFullRuidoAmbiente())) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(g.getGraphicBlack(), "PNG", baos);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            pp.addOneGrafico(bais,
                    "Grafico " + Ponto.ClassificacaoToStringAbrev(ponto.getClassificacao()) + ponto.getNumero() + " - histórico no tempo do nível sonoro do Ponto " + ponto.getNumero());
        } else {
            ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
            ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
            ImageIO.write(g.getGraphicColored(), "PNG", baos1);
            ImageIO.write(g.getGraphicOnlyResidual(), "PNG", baos2);
            ByteArrayInputStream bais1 = new ByteArrayInputStream(baos1.toByteArray());
            ByteArrayInputStream bais2 = new ByteArrayInputStream(baos2.toByteArray());

            pp.addTwoGraficos(
                    bais1,
                    "Grafico " + Ponto.ClassificacaoToStringAbrev(ponto.getClassificacao()) + ponto.getNumero() + "-a - histórico no tempo do nível sonoro do Ponto " + ponto.getNumero(),
                    bais2,
                    "Grafico " + Ponto.ClassificacaoToStringAbrev(ponto.getClassificacao()) + ponto.getNumero() + "-b - histórico no tempo do nível sonoro do Ponto " + ponto.getNumero()
            );
        }

        p.getDocument().insertNewParagraph(p.getCTP().newCursor()).createRun().addBreak(BreakType.PAGE);
    }
}
