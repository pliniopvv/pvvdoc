/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc.gui.list;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author PLINIO
 */
public class ListSelectorController implements Initializable {

    @FXML
    private Label lb_titulo;
    @FXML
    private ListView<String> lista;
    //
    private static String selectedString = null;
    private static String titulo = null;
    private static List<String> listaStrings;

    public static void setNecessaryItens(String _titulo, List<String> _listaStrings) {
        titulo = _titulo;
        listaStrings = _listaStrings;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lb_titulo.setText(titulo);
        lista.getItems().addAll(listaStrings);
    }

    @FXML
    private void onSelectList(MouseEvent event) {

    }

    @FXML
    private void onSelecionar(ActionEvent event) {
        selectedString = lista.getSelectionModel().getSelectedItem();
        ListSelector.getStage().close();
    }

    @FXML
    private void onCancelar(ActionEvent event) {
        ListSelector.getStage().close();
    }

    public static String getSelectedString() {
        return selectedString;
    }

}
