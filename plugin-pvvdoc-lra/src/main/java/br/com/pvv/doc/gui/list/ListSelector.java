/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc.gui.list;

import br.com.pvv.err.LogFX;
import java.util.List;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author PLINIO
 */
public class ListSelector extends Application {

    private String titulo;
    private List<String> lista;
    //
    private static Stage stage;

    public static Stage getStage() {
        return stage;
    }

    public static void setStage(Stage stage) {
        ListSelector.stage = stage;
    }

    public ListSelector(String titulo, List<String> lista) {
        this.titulo = titulo;
        this.lista = lista;
        start(new Stage());
    }

    public static String getSelectedString() {
        return ListSelectorController.getSelectedString();
    }

    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;
        try {
            FXMLLoader.getDefaultClassLoader();
            ListSelectorController.setNecessaryItens(titulo, lista);
            Parent root = FXMLLoader.load(getClass().getResource("ListSelector.fxml"));
            Scene scene = new Scene(root);

            primaryStage.setTitle(titulo);
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.showAndWait();
        } catch (Exception e) {
            LogFX.alert(e);
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
