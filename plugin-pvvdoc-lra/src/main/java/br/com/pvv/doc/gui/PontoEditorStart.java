/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc.gui;

import br.com.pvv.doc.lra.ExecutablePlugin;
import br.com.pvv.err.LogFX;
import br.com.pvv.pvvdoc.plugin.lra.DbTraitAdapter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author GanGss
 */
public class PontoEditorStart extends Application {

    private static Stage stage;
    private static PontoEditorController controller;
    private static DbTraitAdapter pm;

    @Override
    public void start(Stage stage) {
        try {
            this.stage = stage;
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/PontoEditor.fxml"));
            Scene scene = new Scene(root);
            stage.setTitle("Editor de pontos.");
            stage.getIcons().add(new Image(getClass().getResourceAsStream("/br/com/pvv/imgs/logo.png")));
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setOnCloseRequest((WindowEvent event) -> {
//            System.exit(0);
            });
            stage.show();

//            Platform.runLater(() -> {
//                try {
//                    DbTraitAdapter pm = new DbTraitAdapter();
//
//                    // MODIFICADO :: SELECIONAR PASTA COM OS ARQUIVOS;
////                DirectoryChooser dc = new DirectoryChooser();
////                dc.setTitle("Selecione a pasta de extração dos arquivos da amostragem tratados.");
////                File f = dc.showDialog(stage);
////                pm.parse(f);
////                pm.parse(new File("Dados"));
//                    //
////                this.pm = pm;
////                getController().setPontosManeger(pm);
//                } catch (Exception ex) {
//                    LogFX.alert(ex);
////                Logger.getLogger(StartRelatorioClient.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            });
        } catch (IOException ex) {
            LogFX.alert(ex);
            Logger.getLogger(PontoEditorStart.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Stage getStage() {
        return stage;
    }

    public static void setController(PontoEditorController controller) {
        PontoEditorStart.controller = controller;
    }

    public static PontoEditorController getController() {
        return controller;
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static DbTraitAdapter getPontosManeger() {
        return pm;
    }

    public static void setPontosManeger(DbTraitAdapter pm) {
        PontoEditorStart.pm = pm;
    }

    private static List<ExecutablePlugin> observablePlugin = new ArrayList<ExecutablePlugin>();

    public static List<ExecutablePlugin> getObservablePlugin() {
        return observablePlugin;
    }

}
