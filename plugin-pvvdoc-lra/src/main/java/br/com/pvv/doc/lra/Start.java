/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc.lra;

import br.com.pvv.doc.gui.PontoEditorStart;
import br.com.pvv.doc.mem.RelatorioMemory;
import br.com.pvv.doc.model.PontoFactory;
import br.com.pvv.err.LogFX;
import br.com.pvv.pvvdoc.plugin.lra.Ponto;
import br.com.pvv.pvvdoc.plugin.lra.PontosManager;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

/**
 *
 * @author Plinio
 */
public class Start extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        PontoEditorStart pes = new PontoEditorStart();
        ExecutablePlugin ep = new ExecutablePlugin() {
            @Override
            public void execute() throws Exception {
                PontosManager pm = RelatorioMemory.getRelatorio().getPontosManeger();
                DirectoryChooser dc = new DirectoryChooser();
                List<PontoFactory> lps = new ArrayList<PontoFactory>();
                Platform.runLater(() -> {
                    File dir = dc.showDialog(null);
                    for (Ponto p : pm.getPontos()) {
                        lps.add(new PontoFactory(dir, p));
                    }
                    Thread t = new Thread(() -> {
                        lps.forEach(ps -> {
                            try {
                                ps.save();
                            } catch (Exception e) {
                                LogFX.alert(e);
                            }
                        });
                    });
                    t.start();
                });
            }
        };
        PontoEditorStart.getObservablePlugin().add(ep);
        pes.start(new Stage());
    }
}
