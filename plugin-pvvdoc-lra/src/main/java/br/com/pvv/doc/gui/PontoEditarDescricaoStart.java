/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc.gui;

import br.com.pvv.err.LogFX;
import br.com.pvv.pvvdoc.plugin.lra.Ponto;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author GanGss
 */
public class PontoEditarDescricaoStart extends Application {

    private static Stage stage;
    private static Ponto ponto;

    public static Ponto getPonto() {
        return ponto;
    }

    public static void setPonto(Ponto ponto) {
        PontoEditarDescricaoStart.ponto = ponto;
    }

    @Override
    public void start(Stage stage) {
        this.stage = stage;
        try {

            Parent root = FXMLLoader.load(getClass().getResource("/fxml/PontoEditarDescricao.fxml"));
            Scene scene = new Scene(root);
            stage.getIcons().add(new Image(getClass().getResourceAsStream("/br/com/pvv/imgs/logo.png")));
//            stage.setTitle("Editor de descrições.");
            stage.setScene(scene);
            stage.show();

        } catch (IOException ex) {
            LogFX.alert(ex);
            Logger.getLogger(PontoEditarDescricaoStart.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Stage getStage() {
        return stage;
    }

}
