/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc.mem;

import br.com.pvv.pvvdoc.plugin.lra.Relatorio;

/**
 *
 * @author GanGss
 */
public class RelatorioMemory {

    private static Relatorio relatorio = null;

    public static Relatorio getRelatorio() {
        return relatorio;
    }

    public static void setRelatorio(Relatorio relatorio) {
        RelatorioMemory.relatorio = relatorio;
    }

}
