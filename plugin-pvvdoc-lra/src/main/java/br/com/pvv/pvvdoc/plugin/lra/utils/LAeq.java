/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.pvvdoc.plugin.lra.utils;

import br.com.pvv.pvvdoc.plugin.lra.Leitura;
import java.text.DecimalFormat;

/**
 *
 * @author GanGss
 */
public class LAeq {

    public static double CalcLAeq(Leitura[] leituras) {
        if (leituras.length == 0) {
            return 0.0;
        }

        long sum = 0;
        for (Leitura leitura : leituras) {
            sum += Math.pow(10, (leitura.getLAeq() / 10));
        }

        double result = Math.log10((1.0 / leituras.length) * sum) * 10;

//        if (result == Double.NaN) {
//            return 0.0;
//        }
        DecimalFormat df = new DecimalFormat("#.00");

//        return result;
        return Double.parseDouble(df.format(result).replace(",", "."));
    }
}
