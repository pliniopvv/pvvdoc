/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.pvvdoc.plugin.lra;

import br.com.pvv.pvvdoc.plugin.lra.utils.Grafico;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import javax.imageio.ImageIO;

/**
 *
 * @author PLINIO
 */
public class DbTraitAdapter extends PontosManager implements Serializable {

    public DbTraitAdapter() {
    }

    public PontosManager parseDir(File f) throws Exception {
        if (!f.isDirectory()) {
            throw new Exception("Arquivo não é um diretório.");
        }

        File[] files = f.listFiles();
        ArrayList<File> arquivos = new ArrayList<File>();

        for (File arquivo : files) {
            if (!arquivo.isDirectory()) {
                if (arquivo.getName().indexOf(".000") != -1) {
                    arquivos.add(arquivo);
                } else if (arquivo.getName().indexOf(".001") != -1) {
                    arquivos.add(arquivo);
                }
            }
        }

        // Verificador da duplicação dos arquivos, a procura da existência do ".001" ...
        HashSet nomeArquivos = new HashSet();
        for (File arquivo : arquivos) {
            String nome = arquivo.getName().replace(".000", "").replace(".001", "");
            nomeArquivos.add(nome);
        }

        //
        ArrayList<Ponto> pontos = new ArrayList<Ponto>();

        if (this.pontos != null) {
            for (Ponto p : this.pontos) {
                pontos.add(p);
            }
        }

        Iterator i = nomeArquivos.iterator();

        //Debug
//        System.out.println(f.getAbsolutePath() + "\\");
        while (i.hasNext()) {
            String nomeArquivo = (String) i.next();

            File f000 = new File(f.getAbsolutePath() + "\\" + nomeArquivo + ".000");
            File f001 = new File(f.getAbsolutePath() + "\\" + nomeArquivo + ".001");

            Ponto ponto = new Ponto();
            //Debug
//            System.out.print("Nome Arquivo: " + nomeArquivo);
//            System.out.println(" 001 Exist: " + f001.exists());

            if (f001.exists()) {
                ponto.loadWithCodes(f001);
            } else {
                ponto.load(f000);
            }

            pontos.add(ponto);
        }

        this.pontos = pontos.toArray(new Ponto[pontos.size()]);
        return this;
    }

}
