/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.pvvdoc.plugin.lra;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author GanGss
 */
public class FotosManeger implements Serializable {

    List<Foto> fotos = new ArrayList();

    public List<Foto> getFotos() {
        return fotos;
    }

    public void setFotos(List<Foto> fotos) {
        this.fotos = fotos;
    }

    public void addFoto(Foto foto) {
        fotos.add(foto);
        fotos = fotos.stream().sorted().collect(Collectors.toList());
    }

    public void addFoto(List<Foto> foto) {
        foto.stream().forEach((f) -> this.addFoto(f));
    }

    public void remFoto(Foto foto) {
        fotos.remove(foto);
        recontar();
    }

    public void recontar() {
        Foto.setContador(0);
        fotos.stream().sorted().forEach((c) -> c.renumerate());
    }

}
