/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.pvvdoc.plugin.lra.utils;

import br.com.pvv.pvvdoc.plugin.lra.Ponto;
import java.util.Comparator;

/**
 *
 * @author PLINIO
 */
public class PontoComparatorNameFile implements Comparator<Ponto> {

    @Override
    public int compare(Ponto p1, Ponto p2) {
        return p1.getNome().compareTo(p2.getNome());
    }

}
