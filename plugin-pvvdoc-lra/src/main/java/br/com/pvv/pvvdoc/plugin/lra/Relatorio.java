/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.pvvdoc.plugin.lra;

import java.io.Serializable;
import java.util.Calendar;

public class Relatorio implements Serializable {

    private NCA nca;
    private PontosManager pm;
    private KmlAdapter gpsmanager;
    private Empresa empresa;
    private Calendar dataAmostragem;
    private FotosManeger fotoManeger;
    //
    private byte[] capa;
    private byte[] croquiDosPontos;
    private byte[] croquiDoLocal;
    //
    private String conclusao;
    private String funcionarioResponsavelAcompanhamento;
    private String funcionarioResponsavelRelatorio;
    private String numero;

    public String getConclusao() {
        if (conclusao == null) {
            return "Em conformidade com a NBR 10151/2000 o empreendimento \\input{temp/interessado.tex} está situado em \\textbf{área predominantemente industrial}, na qual o Nível de Critério de Avaliação – NCA - para ambientes externos, em dB (A), é \\textbf{70 dB} para o período diurno e \\textbf{60 dB} para o período noturno (Tabela 2). \n"
                    + "\n"
                    + "Considera-se, também para esta análise a Lei Estadual 10.100, de 17 de janeiro de 1990.\n"
                    + "\n"
                    + "Não foram realizadas amostragens noturnas uma vez que o empreendimento não opera neste período.\n"
                    + "\n"
                    + "De acordo com os resultados obtidos e apresentados na Tabela 01, nas Tabelas de 03 a 12 e nos Gráficos de 01 a 12, todos os pontos de amostragem, após tratamento dos dados e exclusão do ruido ambiente, quando aplicável, \\textbf{apresentaram resultados dentro dos limites estabelecidos pelas normatizações vigentes.}\n"
                    + "\n"
                    + "Conclui-se, portanto, que o ruído emitido pelo empreendimento em análise, nas datas e condições supra citadas, \\textbf{está em conformidade com a NBR 10.151 / 2000} \\\\";
        }

        return conclusao;
    }

    public void setConclusao(String conclusao) {
        this.conclusao = conclusao;
    }

    public byte[] getCapa() {
        return capa;
    }

    public void setCapa(byte[] capa) {
        this.capa = capa;
    }

    public byte[] getCroquiDosPontos() {
        return croquiDosPontos;
    }

    public void setCroquiDosPontos(byte[] croquiDosPontos) {
        this.croquiDosPontos = croquiDosPontos;
    }

    public byte[] getCroquiDoLocal() {
        return croquiDoLocal;
    }

    public void setCroquiDoLocal(byte[] croquiDoLocal) {
        this.croquiDoLocal = croquiDoLocal;
    }

    public FotosManeger getFotosManeger() {
        if (fotoManeger == null) {
            fotoManeger = new FotosManeger();
        }

        return fotoManeger;
    }

    public void setFotosManeger(FotosManeger fotoManeger) {
        this.fotoManeger = fotoManeger;
    }

    public String getFuncionarioResponsavelAcompanhamento() {
        return funcionarioResponsavelAcompanhamento;
    }

    public void setFuncionarioResponsavelAcompanhamento(String funcionarioResponsavelAcompanhamento) {
        this.funcionarioResponsavelAcompanhamento = funcionarioResponsavelAcompanhamento;
    }

    public String getFuncionarioResponsavelRelatorio() {
        return funcionarioResponsavelRelatorio;
    }

    public void setFuncionarioResponsavelRelatorio(String funcionarioResponsavelRelatorio) {
        this.funcionarioResponsavelRelatorio = funcionarioResponsavelRelatorio;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Calendar getDataAmostragem() {
        return dataAmostragem;
    }

    public void setDataAmostragem(Calendar dataAmostragem) {
        this.dataAmostragem = dataAmostragem;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public NCA getNca() {
        return nca;
    }

    public void setNca(NCA nca) {
        this.nca = nca;
    }

    public PontosManager getPontosManeger() {
        return pm;
    }

    public void setPontosManeger(PontosManager pontosManeger) {
        this.pm = pontosManeger;
    }

    public KmlAdapter getGpsManager() {
        return gpsmanager;
    }

    public void setGpsManeger(KmlAdapter gpsmanager) {
        this.gpsmanager = gpsmanager;
    }
}
