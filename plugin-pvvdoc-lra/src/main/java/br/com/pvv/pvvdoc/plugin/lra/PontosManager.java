/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.pvvdoc.plugin.lra;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author PLINIO
 */
public class PontosManager implements Serializable {

    public Ponto[] pontos = null;

    public Ponto[] getPontos() {
        return pontos;
    }

    public void setPontos(Ponto[] pontos) {
        this.pontos = pontos;
    }

    public Ponto[] getPontos(int classificacao) {
        ArrayList<Ponto> p = new ArrayList<Ponto>();
        for (Ponto ponto : pontos) {
            if (ponto.getClassificacao() == classificacao) {
                p.add(ponto);
            }
        }
        Ponto[] retorno = p.toArray(new Ponto[p.size()]);
        Arrays.sort(retorno);
        return retorno;
    }

    public boolean hasPontos(int classificacao) {
        Ponto[] pontos = getPontos(classificacao);
        if (pontos.length >= 1) {
            return true;
        } else {
            return false;
        }
    }

    public int size() {
        return pontos.length;
    }

}
