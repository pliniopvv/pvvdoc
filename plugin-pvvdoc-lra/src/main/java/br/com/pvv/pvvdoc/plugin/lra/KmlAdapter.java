/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.pvvdoc.plugin.lra;

import br.com.pvv.err.LogFX;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author GanGss
 */
public class KmlAdapter implements Serializable {

    private File file;
    private List<Gps> gps;
//    private ArrayList<Gps> agps = new ArrayList<Gps>();

//    public static void main(String[] args) {
////        File f = new File("C:\\Users\\GanGss\\Documents\\dadosgps.kml");
//        File f = new File("C:\\Users\\GanGss\\Desktop\\gps.kml");
//        GpsManeger gf = new GpsManeger();
//        gf.parse(f);
//
//        for (Gps gps : gf.getGpss()) {
//            System.out.println(gps.getNome());
//            System.out.println("lat grau > " + gps.getLatGrau());
//            System.out.println("lat min > " + gps.getLatMin());
//            System.out.println("lat sec > " + gps.getLatSeconds());
//            System.out.println("lat dir > " + gps.getParalelo());
//            System.out.println("long grau > " + gps.getLongGrau());
//            System.out.println("long min > " + gps.getLongMin());
//            System.out.println("long sec > " + gps.getLongSeconds());
//            System.out.println("long dir > " + gps.getMeridiano());
//
//        }
//
//    }
    public KmlAdapter() {
    }

    public void parse(File file) {
        try {
            this.file = file;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(file);

            ArrayList<Gps> agps = new ArrayList<Gps>();

            NodeList nl = doc.getElementsByTagName("Placemark");

            for (int i = 0; i < nl.getLength(); i++) {
                Element n = (Element) nl.item(i);
                NodeList nl2 = n.getChildNodes();
                Gps gps = new Gps();

                Node nome = n.getElementsByTagName("name").item(0);
                Node coord = n.getElementsByTagName("coordinates").item(0);
//                Node longitude = n.getElementsByTagName("longitude").item(0);
                String longitude = coord.getTextContent().split(",")[0];
                String latitude = coord.getTextContent().split(",")[1];

                gps.setNome(nome.getTextContent());
                gps.setLatitude(Double.parseDouble(latitude));
                gps.setLongitude(Double.parseDouble(longitude));

                agps.add(gps);
                this.gps = agps;
            }
        } catch (SAXException ex) {
            LogFX.alert(ex);
            Logger.getLogger(KmlAdapter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            LogFX.alert(ex);
            Logger.getLogger(KmlAdapter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            LogFX.alert(ex);
            Logger.getLogger(KmlAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Gps[] getGpss() {
        return gps.toArray(new Gps[gps.size()]);
    }

    public List<Gps> getGps() {
        return gps;
    }

    public void setGps(List<Gps> gps) {
        this.gps = gps;
    }

}
