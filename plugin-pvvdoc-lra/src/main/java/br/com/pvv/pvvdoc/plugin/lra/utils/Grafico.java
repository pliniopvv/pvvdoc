package br.com.pvv.pvvdoc.plugin.lra.utils;

import br.com.pvv.pvvdoc.plugin.lra.DbTraitAdapter;
import br.com.pvv.pvvdoc.plugin.lra.Leitura;
import br.com.pvv.pvvdoc.plugin.lra.Ponto;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import javax.imageio.ImageIO;

public class Grafico {

    int yMin = 0;
    int yMax = 325;
    //
    private Ponto pontos;

    public Grafico(Ponto ponto) {
        this.pontos = ponto;
    }

    public BufferedImage getBackground() {
        BufferedImage bi = new BufferedImage(500, 375, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = (Graphics2D) bi.getGraphics();

        g.setColor(Color.WHITE);
        g.fillRect(0, 0, 500, 375);
        g.setColor(Color.BLACK);
        g.drawLine(50, 0, 50, 325);
        g.drawLine(50, 325, 500, 325);
//        g.drawRect(0, 0, 499, 374);
        g.drawRect(50, 0, 449, 325);

        buildEixoX(g);

        // EIXO Y
        double maxLAeq = Arrays.asList(pontos.getLeituras())
                .stream()
                .mapToDouble(c -> c.getLAeq())
                .max().orElse(0.0);

        for (int i = 0; i <= 130; i++) {
            if ((i % 10) == 0) {
                tickY(g, i);
            }
        }

        // LEGENDA DO EIXO X
        Font font = new Font(null, Font.BOLD, 20);
        g.setFont(font);
        g.drawString("Tempo", 250, 358);
        // LEGENDA DO EIXO Y
        AffineTransform affineTransform = new AffineTransform();
        affineTransform.rotate(Math.toRadians(-90), 0, 0);
        Font rotatedFont = font.deriveFont(affineTransform);
        g.setFont(rotatedFont);
//        g.drawString("db(A)", 50, 50);
        g.drawString("db(A)", 25, 187);
        // FINALIZANDO
        g.dispose();
        return bi;
    }

    public BufferedImage getGraphicBlack() {
        BufferedImage bi = getBackground();
        Graphics2D g = bi.createGraphics();
        g.setColor(Color.BLACK);

        // COLOCAR PONTOS ::
        List<Leitura> leituras = Arrays.asList(pontos.getLeituras());

        Leitura old = null;
        for (int i = 0; i < leituras.size(); i++) {
            tickBlackLAeq(g, leituras.get(i), i);
//            tickBlackLAeq(g, leituras.get(i).getLAeq(), i);
            if (old != null) {
                tickBlackToBar(g, old, leituras.get(i), i);
//                tickBlackToBar(g, old.getLAeq(), leituras.get(i).getLAeq(), i);
            }
            old = leituras.get(i);
        }

        g.dispose();
        return bi;
    }

    public BufferedImage getGraphicColored() {
        BufferedImage bi = getBackground();
        Graphics2D g = bi.createGraphics();
        g.setColor(Color.BLACK);

        // COLOCAR PONTOS ::
        List<Leitura> leituras = Arrays.asList(pontos.getLeituras());

        Leitura old = null;
        for (int i = 0; i < leituras.size(); i++) {
            tickColoredLAeq(g, leituras.get(i), i);
            if (old != null) {
                tickColoredToBar(g, old, leituras.get(i), i);
            }
            old = leituras.get(i);
        }

        g.dispose();
        return bi;
    }

    public BufferedImage getGraphicOnlyResidual() {
        BufferedImage bi = getBackground();
        Graphics2D g = bi.createGraphics();
        g.setColor(Color.BLACK);

        // COLOCAR PONTOS ::
        List<Leitura> leituras = Arrays.asList(pontos.getLeituras());

        Leitura old = null;
        for (int i = 0; i < leituras.size(); i++) {
            if ((leituras.get(i).getCodigo() != Leitura.RESIDUAL) && (leituras.get(i).getCodigo() != Leitura.AMBIENTAL)) {
                old = null;
                continue;
            }

            tickColoredLAeq(g, leituras.get(i), i);
            if (old != null) {
                tickColoredToBar(g, old, leituras.get(i), i);
            }
            old = leituras.get(i);
        }

        g.dispose();
        return bi;
    }

//    private void tickBlackToBar(Graphics2D g, double oldLAeq, double nextLAeq, int index) {
//        double barra_leitura = (450.0 / pontos.getLeituras().length);
//        int x = 50 + (int) (barra_leitura * index);
//
//        int LAeqMax = ((int) ((pontos.getLAeqMax() / 10) + 1)) * 10;
//        int LAeqMin = ((int) (pontos.getLAeqMin() / 10)) * 10;
//
//        double coef = yMax / (LAeqMax - LAeqMin);
//        int yi = (int) (yMax - (oldLAeq * coef));
//        int yf = (int) (yMax - (nextLAeq * coef));
//
//        g.drawLine(x, yi, x, yf);
//    }
////
//    private void tickBlackLAeq(Graphics2D g, double LAeq, int index) {
//        double barra_leitura = (450.0 / pontos.getLeituras().length);
//        int x = 50 + (int) (barra_leitura * index);
//
//        int LAeqMax = ((int) ((pontos.getLAeqMax() / 10) + 1)) * 10;
//        int LAeqMin = ((int) (pontos.getLAeqMin() / 10)) * 10;
//
//        double coef = yMax / (LAeqMax - LAeqMin);
//        int y = (int) (yMax - ((LAeq - LAeqMin) * coef));
//
////        System.out.println(x);
//        g.drawLine(x, y, x + (int) barra_leitura, y);
//    }
    private void buildEixoX(Graphics2D g) {
        int div = pontos.getLeituras().length / 6;
        double barra_leitura = (450.0 / pontos.getLeituras().length);

        List<Leitura> leituras = Arrays.asList(pontos.getLeituras());

        Font fonte = new Font("Arial", Font.PLAIN, 9);
        FontMetrics fm = g.getFontMetrics(fonte);
        g.setFont(fonte);

        // EIXO X
        for (int i = 0; i <= leituras.size(); i++) {
            double xDouble = 50.0 + barra_leitura * i + (barra_leitura / 2.0) - 1.0;
            int x = (int) xDouble;
            int y = 325;
            if ((i % div == 0)) {
                dotTickX(g, x);
                g.drawLine(
                        x,
                        y,
                        x,
                        y + 5);
                y += 12;

                if ((i == 0) || (i == 180)) {
                    continue;
                }

                String horario = DateAdapter.HorarioCalendar(leituras.get(i).getHorario());
                x -= fm.stringWidth(horario) / 2;
                g.drawString(horario, x, y);
            } else if ((i % (div / 6) == 0)) {
                g.drawLine(
                        x,
                        y,
                        x,
                        y + 2
                );
            }
        }
    }

    private void tickY(Graphics2D g, int LAeq) {
//        int yMin = 0;
//        int yMax = 325;

        int LAeqMax = ((int) ((pontos.getLAeqMax() / 10) + 1)) * 10;
        int LAeqMin = ((int) (pontos.getLAeqMin() / 10)) * 10;

        double coef = yMax / (LAeqMax - LAeqMin);

        int val = (int) (yMax - ((LAeq - LAeqMin) * coef));

        Font fonte = new Font("Arial", Font.PLAIN, 9);
        FontMetrics fm = g.getFontMetrics(fonte);

        g.drawLine(50 - 5, val, 50, val);
        dotTickY(g, val);
        g.drawString(String.valueOf(LAeq), 50 - 2 - fm.stringWidth(String.valueOf(LAeq)), val + 9);
    }

    private void dotTickY(Graphics2D g, int val) {
        float dash1[] = {5.0f};
        BasicStroke dashed = new BasicStroke(1.0f,
                BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER,
                10.0f, dash1, 0.0f);
        g.setStroke(dashed);
        g.drawLine(50, val, 500, val);
    }

    private void dotTickX(Graphics2D g, int val) {
        float dash1[] = {5.0f};
        BasicStroke dashed = new BasicStroke(1.0f,
                BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER,
                10.0f, dash1, 0.0f);
        g.setStroke(dashed);
        g.drawLine(val, 0, val, 325);
    }

    private void tickColoredLAeq(Graphics2D g, Leitura leitura, int index) {
        double LAeq = leitura.getLAeq();

        switch (leitura.getCodigo()) {
            case Leitura.CODIGO_1:
                g.setColor(Color.RED);
                break;
            case Leitura.CODIGO_2:
                g.setColor(Color.GREEN);
                break;
            case Leitura.CODIGO_3:
                g.setColor(Color.BLUE);
                break;
            default:
                g.setColor(Color.BLACK);
                break;
        }

        double barra_leitura = (450.0 / pontos.getLeituras().length);
        int x = 50 + (int) (barra_leitura * index);

        int LAeqMax = ((int) ((pontos.getLAeqMax() / 10) + 1)) * 10;
        int LAeqMin = ((int) (pontos.getLAeqMin() / 10)) * 10;

        double coef = yMax / (LAeqMax - LAeqMin);
        int y = (int) (yMax - ((LAeq - LAeqMin) * coef));

        g.drawLine(x, y, x + (int) barra_leitura, y);
    }

    private void tickBlackLAeq(Graphics2D g, Leitura leitura, int index) {
        double LAeq = leitura.getLAeq();

        g.setColor(Color.BLACK);

        double barra_leitura = (450.0 / pontos.getLeituras().length);
        int x = 50 + (int) (barra_leitura * index);

        int LAeqMax = ((int) ((pontos.getLAeqMax() / 10) + 1)) * 10;
        int LAeqMin = ((int) (pontos.getLAeqMin() / 10)) * 10;

        double coef = yMax / (LAeqMax - LAeqMin);
        int y = (int) (yMax - ((LAeq - LAeqMin) * coef));

        g.drawLine(x, y, x + (int) barra_leitura, y);
    }

    private void tickColoredToBar(Graphics2D g, Leitura oldLeitura, Leitura nextLeitura, int index) {
        double oldLAeq = oldLeitura.getLAeq();
        double nextLAeq = nextLeitura.getLAeq();

        if (oldLeitura.getCodigo() != nextLeitura.getCodigo()) {
            switch (nextLeitura.getCodigo()) {
                case Leitura.CODIGO_1:
                    g.setColor(Color.RED);
                    break;
                case Leitura.CODIGO_2:
                    g.setColor(Color.GREEN);
                    break;
                case Leitura.CODIGO_3:
                    g.setColor(Color.BLUE);
                    break;
                default:
                    g.setColor(Color.BLACK);
                    break;
            }
        }

        double barra_leitura = (450.0 / pontos.getLeituras().length);
        int x = 50 + (int) (barra_leitura * index);

        int LAeqMax = ((int) ((pontos.getLAeqMax() / 10) + 1)) * 10;
        int LAeqMin = ((int) (pontos.getLAeqMin() / 10)) * 10;

        double coef = yMax / (LAeqMax - LAeqMin);

        int yi = (int) (yMax - ((oldLAeq - LAeqMin) * coef));
        int yf = (int) (yMax - ((nextLAeq - LAeqMin) * coef));

        g.drawLine(x, yi, x, yf);
    }

    private void tickBlackToBar(Graphics2D g, Leitura oldLeitura, Leitura nextLeitura, int index) {
        double oldLAeq = oldLeitura.getLAeq();
        double nextLAeq = nextLeitura.getLAeq();

        g.setColor(Color.BLACK);

        double barra_leitura = (450.0 / pontos.getLeituras().length);
        int x = 50 + (int) (barra_leitura * index);

        int LAeqMax = ((int) ((pontos.getLAeqMax() / 10) + 1)) * 10;
        int LAeqMin = ((int) (pontos.getLAeqMin() / 10)) * 10;

        double coef = yMax / (LAeqMax - LAeqMin);

        int yi = (int) (yMax - ((oldLAeq - LAeqMin) * coef));
        int yf = (int) (yMax - ((nextLAeq - LAeqMin) * coef));

        g.drawLine(x, yi, x, yf);
    }

    public static void main(String[] args) throws Exception {
        DbTraitAdapter pm = new DbTraitAdapter();
        pm.parseDir(new File("Z:\\CLIENTES\\Metropolitana\\RELATÓRIO\\RUIDO\\DADOS\\Dados"));
        Grafico g = new Grafico(pm.getPontos()[0]);

        System.out.println("Arquivo:" + pm.getPontos()[0].getNome());
        ImageIO.write(g.getGraphicColored(), "PNG", new File("FOTO.PNG"));
    }
}
