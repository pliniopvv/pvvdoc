/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.pvvdoc.plugin.lra;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.imageio.ImageIO;

/**
 *
 * @author PLINIO
 */
public class Foto implements Comparable<Foto>, Serializable {

    private byte[] arquivo;
    private String extensao;
    private String legenda;
    private int ponto;
    private Boolean primeira;
    private Boolean ultima;
    //
    private static int contador = 0;

    public Foto(File file) throws IOException {
        BufferedImage i = ImageIO.read(file);
        BufferedImage ni = new BufferedImage(i.getWidth(), i.getHeight(), i.getType());
        Graphics2D g = (Graphics2D) ni.getGraphics();
        g.drawImage(i, 0, 0, i.getWidth(), i.getHeight(), null);
        g.dispose();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(ni, file.getName().split("\\.")[1], baos);
        baos.flush();
        this.arquivo = baos.toByteArray();

//        this.arquivo = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
        this.extensao = file.getName().split("\\.")[1];
        contador++;
        ponto = contador;
        legenda = "Imagem do ponto " + ponto;
    }

    public Boolean getPrimeira() {
        return primeira;
    }

    public void setPrimeira(Boolean primeira) {
        this.primeira = primeira;
    }

    public Boolean getUltima() {
        return ultima;
    }

    public void setUltima(Boolean ultima) {
        this.ultima = ultima;
    }

    public int getPonto() {
        return ponto;
    }

    public void setNumPonto(int ponto) {
        this.ponto = ponto;
    }

    public String getExtensao() {
        return extensao;
    }

    public void setExtensao(String extensao) {
        this.extensao = extensao;
    }

    public String getLegenda() {
        return legenda;
    }

    public byte[] getArquivo() {
        return arquivo;
    }

    public void setArquivo(byte[] arquivo) {
        this.arquivo = arquivo;
    }

    public void setLegenda(String legenda) {
        this.legenda = legenda;
    }

    public String getNome() {
//        return arquivo.getName();
        return "PONTO" + ponto + "." + getExtensao();
    }

    public void renumerate() {
        contador++;
        ponto = contador;
    }

    public static int getContador() {
        return contador;
    }

    public static void setContador(int contador) {
        Foto.contador = contador;
    }

    @Override
    public int compareTo(Foto t) {
        if (this.getPonto() == t.getPonto()) {
            return 0;
        } else if (this.getPonto() < t.getPonto()) {
            return -1;
        } else {
            return 1;
        }
    }

}
