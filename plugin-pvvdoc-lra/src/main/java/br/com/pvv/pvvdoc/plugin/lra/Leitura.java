/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.pvvdoc.plugin.lra;

import java.io.Serializable;
import java.util.Calendar;

/**
 *
 * @author PLINIO
 */
public class Leitura implements Serializable {

    public static final int CODIGO_1 = 0;
    public static final int CODIGO_2 = 1;
    public static final int CODIGO_3 = 2;
    public static final int RESIDUAL = 3;
    public static final int AMBIENTAL = 4;
    //
    public static final String CODIGO_1s = "Código 1";
    public static final String CODIGO_2s = "Código 2";
    public static final String CODIGO_3s = "Código 3";
    public static final String RESIDUALs = "Residual";
    public static final String AMBIENTALs = "Ambiental";
    //

    //
    private Calendar Horario;
    private int Duracao;
    //
    private int codigo;
    private double LAeq;
    private double Lmin;
    private double Lmax;

    public Leitura() {
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public double getLAeq() {
        return LAeq;
    }

    public void setLAeq(double Leq) {
        this.LAeq = Leq;
    }

    public double getLmin() {
        return Lmin;
    }

    public void setLmin(double Lmin) {
        this.Lmin = Lmin;
    }

    public double getLmax() {
        return Lmax;
    }

    public void setLmax(double Lmax) {
        this.Lmax = Lmax;

    }

    public Calendar getHorario() {
        return Horario;
    }

    public void setHorario(Calendar Horario) {
        this.Horario = Horario;
    }

    public int getDuracao() {
        return Duracao;
    }

    public void setDuracao(int Duracao) {
        this.Duracao = Duracao;
    }

}
