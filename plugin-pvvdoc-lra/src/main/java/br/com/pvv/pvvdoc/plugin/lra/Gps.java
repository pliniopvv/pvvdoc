/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.pvvdoc.plugin.lra;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 *
 * @author GanGss
 */
public class Gps implements Serializable {

    private double latitude;
    private double longitude;
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitute) {
        this.latitude = latitute;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getLatGrau() {
        int grau = (int) this.latitude;
        return Math.abs(grau);
    }

    public int getLongGrau() {
        int grau = (int) this.longitude;
        return Math.abs(grau);
    }

    public int getLatMin() {
        double min = Math.abs(latitude) - getLatGrau();
        min = Math.abs(min * 60.0);
        return (int) min;
    }

    public int getLongMin() {
        double min = Math.abs(longitude) - getLongGrau();
        min = Math.abs(min * 60.0);
        return (int) min;
    }

    public double getLatSeconds() {
        double min = Math.abs(latitude) - getLatGrau();
        min = Math.abs(min * 60.0);
        double second = min - ((int) (min));
        second = second * 60;
        DecimalFormat df = new DecimalFormat("#.00");
        return Double.parseDouble(df.format(second).replace(",", "."));
    }

    public double getLongSeconds() {
        double min = Math.abs(longitude) - getLongGrau();
        min = Math.abs(min * 60.0);
        double second = min - ((int) (min));
        second = second * 60;
        DecimalFormat df = new DecimalFormat("#.00");
        return Double.parseDouble(df.format(second).replace(",", "."));
    }

    public String getParalelo() {
        if (this.latitude > 0) {
            return "N";
        }
        return "S";
    }

    public String getMeridiano() {
        if (this.longitude > 0) {
            return "L";
        }
        return "O";
    }

    public String getFullLatitude() {
        return getLatGrau() + "º" + getLatMin() + "'" + getLatSeconds() + "''" + getParalelo();
    }

    public String getFullLongitude() {
        return getLongGrau() + "º" + getLongMin() + "'" + getLongSeconds() + "''" + getMeridiano();
    }
}
