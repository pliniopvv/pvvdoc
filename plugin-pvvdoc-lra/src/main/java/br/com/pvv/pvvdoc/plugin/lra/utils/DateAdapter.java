/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.pvvdoc.plugin.lra.utils;

import br.com.pvv.pvvdoc.plugin.lra.Ponto;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author GanGss
 */
public abstract class DateAdapter {

    public static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public static final SimpleDateFormat sdfData = new SimpleDateFormat("dd/MM/yyyy");
    public static final SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm:ss");
    public static final String[] mes = {"", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"};

    public static LocalDate parseLocalDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        LocalDate retorno = LocalDate.of(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH) + 1,
                cal.get(Calendar.DAY_OF_MONTH));
        return retorno;
    }

    public static LocalDateTime parseLocalDateTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        LocalDateTime retorno = LocalDateTime.of(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH) + 1,
                cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.HOUR),
                cal.get(Calendar.MINUTE)
        );
        return retorno;
    }

    public static String parseDateBR(LocalDate data) {
        if (data == null) {
            return null;
        }
        return data.getDayOfMonth() + "-" + data.getMonthValue() + "-" + data.getYear();
    }

    public static String parseDateDF(LocalDate data) {
        if (data == null) {
            return null;
        }
        return data.getYear() + "-" + data.getMonthValue() + "-" + data.getDayOfMonth();
    }

    public static LocalDate parseLocalDate(String dataDF) {
        String[] d = dataDF.split("-");
        return LocalDate.of(Integer.valueOf(d[0]), Integer.valueOf(d[1]), Integer.valueOf(d[2]));
    }

    public static String parseDateTimeBR(Date data) {
        LocalDateTime ld = parseLocalDateTime(data);
        return parseDateTimeBR(ld);
    }

    public static String parseDateTimeBR(LocalDateTime data) {
        if (data == null) {
            return null;
        }

        return data.getDayOfMonth() + "/" + data.getMonthValue() + "/" + data.getYear() + " " + data.getHour() + "h" + data.getMinute() + "min" + data.getSecond() + "s";
    }

    public static String parseDateTimeDF(LocalDateTime data) {
        if (data == null) {
            return null;
        }

        return data.getYear() + "-" + data.getMonthValue() + "-" + data.getDayOfMonth() + " " + data.getHour() + ":" + data.getMinute() + ":" + data.getSecond();
    }

    public static LocalDateTime parseDateTime(String dataDF) {
        if (dataDF == null) {
            return null;
        }

        String[] dt = dataDF.split(" ");
        String[] d = dt[0].split("-");
        String[] hms = dt[1].split(":");
        return LocalDateTime.of(
                Integer.valueOf(d[0]),
                Integer.valueOf(d[1]),
                Integer.valueOf(d[2]),
                Integer.valueOf(hms[0]),
                Integer.valueOf(hms[1]),
                (int) Double.parseDouble(hms[2]));
    }

//    public static LocalDateTime parseDateTime(String dataDF, String h, String m, String s) {
//
//    }
    public static Date parseDate(LocalDate value) {
        return Date.from(value.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static String CalendarPorExtenso(Calendar data) {
        String dia = String.valueOf(data.get(Calendar.DAY_OF_MONTH));
        String mes = DateAdapter.mes[data.get(Calendar.MONTH)];
        String ano = String.valueOf(data.get(Calendar.YEAR));
        String retorno = dia + " de " + mes + " de " + ano;
        return retorno;
    }

    public static String CalendarReduzido(Calendar data) {
        String dia = String.valueOf(data.get(Calendar.DAY_OF_MONTH));
        String mes = String.valueOf(data.get(Calendar.MONTH) + 1);
        String ano = String.valueOf(data.get(Calendar.YEAR));
        String retorno = dia + "/" + mes + "/" + ano;
        return retorno;
    }

    public static Calendar StringToCalendar(String data) throws ParseException {
        Calendar c = Calendar.getInstance();
        c.setTime(sdf.parse(data));
        return c;
    }

    public static String HorarioCalendar(Calendar data) {
        String horas = String.valueOf(data.get(Calendar.HOUR_OF_DAY));
        String minutos = String.valueOf(data.get(Calendar.MINUTE));
        String segundos = String.valueOf(data.get(Calendar.SECOND));
        String retorno = horas + "h" + minutos + "min" + segundos + "s";
        return retorno;
    }

    public static String getRangeTime(Ponto[] pontos) {
        PontoComparatorCalendar pcc = new PontoComparatorCalendar();
        Arrays.sort(pontos, pcc);

        String primeiro = HorarioCalendar(pontos[0].getInicio());
        String ultimo = HorarioCalendar(pontos[pontos.length - 1].getFim());
        return primeiro + " - " + ultimo;
    }

    public static String CalendarToString(Calendar data) {
        return sdf.format(data.getTime());
    }
}
