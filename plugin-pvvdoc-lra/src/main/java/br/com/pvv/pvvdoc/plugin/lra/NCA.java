/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.pvvdoc.plugin.lra;

import br.com.pvv.pvvdoc.plugin.lra.utils.LAeq;
import java.io.Serializable;
//import relatorio.knl.LAeq;

/**
 *
 * @author PLINIO
 */
public final class NCA implements Serializable {

    public static final int AREA_DE_SITIO_E_FAZENDAS = 0;
    public static final int AREA_ESTRITAMENTE_RESIDENCIA_URBANA_OU_DE_HOSPITAL_OU_ESCOLA = 1;
    public static final int AREA_MISTA_PREDOMINANTEMENTE_RESIDENCIAL = 2;
    public static final int AREA_MISTA_COM_VOCACAO_COMERCIAL_E_ADMINISTRATIVA = 3;
    public static final int AREA_MISTA_COM_VOCACAO_RECREACIONAL = 4;
    public static final int AREA_PREDOMINANTEMENTE_INDUSTRIAL = 5;
    //
    public static final String[] DESC = {
        //        "",
        "AREA DE SITIOS E FAZENDAS",
        "AREA ESTRITAMENTE RESIDENCIAL URBANA OU DE HOSPITAL OU ESCOLA.",
        "AREA MISTA PREDOMINANTEMENTE RESIDENCIAL.",
        "AREA MISTA COM VOCAÇÃO COMERCIAL E ADMINISTRATIVA.",
        "AREA MISTA COM VOCAÇÃO RECREACIONAL.",
        "AREA PREDOMINANTEMENTE INDUSTRIAL."};
    //
    private int limiteManha;
    private int limiteTarde;
    private int limiteNoite;
    private int area;

    public NCA(int AREA) {
        this.area = AREA;
        setAREA(area);
    }

    public void setAREA(int AREA) {
        this.area = AREA;
        switch (AREA) {
            case AREA_DE_SITIO_E_FAZENDAS:
                limiteManha = 40;
                limiteTarde = 40;
                limiteNoite = 35;
                break;
            case AREA_ESTRITAMENTE_RESIDENCIA_URBANA_OU_DE_HOSPITAL_OU_ESCOLA:
                limiteManha = 50;
                limiteTarde = 50;
                limiteNoite = 45;
                break;
            case AREA_MISTA_PREDOMINANTEMENTE_RESIDENCIAL:
                limiteManha = 55;
                limiteTarde = 55;
                limiteNoite = 50;
                break;
            case AREA_MISTA_COM_VOCACAO_COMERCIAL_E_ADMINISTRATIVA:
                limiteManha = 60;
                limiteTarde = 60;
                limiteNoite = 55;
                break;
            case AREA_MISTA_COM_VOCACAO_RECREACIONAL:
                limiteManha = 65;
                limiteTarde = 65;
                limiteNoite = 60;
                break;
            case AREA_PREDOMINANTEMENTE_INDUSTRIAL:
                limiteManha = 70;
                limiteTarde = 70;
                limiteNoite = 60;
                break;
            default:
                limiteManha = -1;
                limiteTarde = -1;
                limiteNoite = -1;
        }
    }

    public int getLimite(int Periodo) {
        int limit;
        switch (Periodo) {
            default:
                limit = this.getLimiteManha();
                break;
            case Ponto.TARDE:
                limit = this.getLimiteTarde();
                break;
            case Ponto.NOITE:
                limit = this.getLimiteNoite();
                break;
        }
        return limit;
    }

    public int getLimiteManha() {
        return limiteManha;
    }

    public int getLimiteTarde() {
        return limiteTarde;
    }

    public int getLimiteNoite() {
        return limiteNoite;
    }

    public int getArea() {
        return area;
    }

    public double compareLraWithLimit(Ponto p1, int Periodo) {
        double Lra = LAeq.CalcLAeq(p1.getLeituras(Leitura.AMBIENTAL));
        double limit = this.getLimite(Periodo);
        return (Lra > limit ? Lra : limit);
    }

    public String getDesc() {
        return this.DESC[area];
    }

//    NEW
//    NEW
//    NEW
//    NEW
//    NEW
//    NEW
//    public double compareLraWithManha(Ponto p1) {
//        double Lra = LAeq.CalcLAeq(p1.getLeituras(Leitura.AMBIENTAL));
//        double limit = this.getLimiteManha();
////        return (Lra > limit ? Lra : limit);
//
//        if (p1.getLeituras(Leitura.RESIDUAL).length == 0) {
//            return Lra;
//        }
//        return limit;
//    }
//
//    public String compareLraWithManhaString(Ponto p1) {
//        double NCAc = compareLraWithManha(p1);
//        return (p1.getLAeqTratado() < NCAc ? "Atende" : "Não Atende");
//    }
//
//    public double compareLraWithTarde(Ponto p1) {
//        double Lra = LAeq.CalcLAeq(p1.getLeituras(Leitura.AMBIENTAL));
//        double limit = this.getLimiteTarde();
////        return (Lra > limit ? Lra : limit);
//
//        if (p1.getLeituras(Leitura.RESIDUAL).length == 0) {
//            return Lra;
//        }
//        return limit;
//    }
//
//    public String compareLraWithTardeString(Ponto p1) {
//        double NCAc = compareLraWithTarde(p1);
//        return (p1.getLAeqTratado() < NCAc ? "Atende" : "Não Atende");
//    }
//
//    public double compareLraWithNoite(Ponto p1) {
//        double Lra = LAeq.CalcLAeq(p1.getLeituras(Leitura.AMBIENTAL));
//        double limit = this.getLimiteNoite();
////        return (Lra > limit ? Lra : limit);
//
//        if (p1.getLeituras(Leitura.RESIDUAL).length == 0) {
//            return Lra;
//        }
//        return limit;
//    }
//
//    public String compareLraWithNoiteString(Ponto p1) {
//        double NCAc = compareLraWithNoite(p1);
//        return (p1.getLAeqTratado() < NCAc ? "Atende" : "Não Atende");
//    }
//    OLD
//    OLD
//    OLD
//    OLD
//    OLD
    public double compareLraWithManha(Ponto p1) {
        double Lra = LAeq.CalcLAeq(p1.getLeituras(Leitura.AMBIENTAL));
        double limit = this.getLimiteManha();
        if (p1.isFullRuidoAmbiente()) {
            return (Lra > limit ? Lra : limit);
        }
        return limit;
    }

    public String compareLraWithManhaString(Ponto p1) {
        double NCAc = compareLraWithManha(p1);
        return (p1.getLAeqTratado() < NCAc ? "Atende" : "Não Atende");
    }

    public double compareLraWithTarde(Ponto p1) {
        double Lra = LAeq.CalcLAeq(p1.getLeituras(Leitura.AMBIENTAL));
        double limit = this.getLimiteTarde();
        if (p1.isFullRuidoAmbiente()) {
            return (Lra > limit ? Lra : limit);
        }
        return limit;
    }

    public String compareLraWithTardeString(Ponto p1) {
        double NCAc = compareLraWithTarde(p1);
        return (p1.getLAeqTratado() < NCAc ? "Atende" : "Não Atende");
    }

    public double compareLraWithNoite(Ponto p1) {
        double Lra = LAeq.CalcLAeq(p1.getLeituras(Leitura.AMBIENTAL));
        double limit = this.getLimiteNoite();
        if (p1.isFullRuidoAmbiente()) {
            return (Lra > limit ? Lra : limit);
        }
        return limit;
    }

    public String compareLraWithNoiteString(Ponto p1) {
        double NCAc = compareLraWithNoite(p1);
        return (p1.getLAeqTratado() < NCAc ? "Atende" : "Não Atende");
    }
}
