/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.pvvdoc.plugin.lra;

import br.com.pvv.pvvdoc.plugin.lra.utils.DateAdapter;
import br.com.pvv.pvvdoc.plugin.lra.utils.LAeq;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author PLINIO
 */
public class Ponto implements Comparable<Ponto>, Serializable {

//    public static final int NULL = -1;
    public static final int MANHA = 0;
    public static final int TARDE = 1;
    public static final int NOITE = 2;

    private static String[] decodificarClassificacao = {"MANHA", "TARDE", "NOITE"};
    private static String[] decodificarClassificacaoAbreviada = {"M", "T", "N"};
    private int classificacao = 0;

    private int numero;
    private String descricao;
    private Leitura[] leituras;
    private Foto foto;
    private String nome;
    // FIGURA
    private String Chamada;
    private String descCod1;
    private String descCod2;
    private String descCod3;
    private String Fechamento;
    private String RuidoAmbiente;
    private String RuidoEmpreendimento;
    // GRÁFICO
//    private String Chamada = "No \\textbf{Gráfico \\ponto{" + this.ClassificacaoToStringAbrev(this.getClassificacao()) + "}-a} é apresentado o histórico no tempo do nível sonoro do \\textbf{Ponto \\ponto{" + this.ClassificacaoToStringAbrev(this.getClassificacao()) + "}} sem tratamento. ";
//    private String descCod1 = "Os trechos na cor vermelha, identificados como \\textbf{Código 1} representam os períodos com incidência de ruído gerado pela passagem de veículos de pequeno porte na rua próxima ao ponto de amostragem. ";
//    private String descCod2 = "Os trechos na cor verde, identificados como \\textbf{Código 2} representam os períodos com incidência de ruído gerado pela passagem de veículos de grande porte na avenida próxima ao ponto de amostragem. ";
//    private String descCod3 = "Os trechos na cor azul, identificados como \\textbf{Código 3} representam os períodos com incidência de ruído ambiente gerado ";
//    private String Fechamento = "O \\textbf{Gráfico " + this.ClassificacaoToStringAbrev(this.getClassificacao()) + "\\ponto{" + this.ClassificacaoToStringAbrev(this.getClassificacao()) + "}-b} apresenta o histórico no tempo do nível sonoro após tratamento dos dados e exclusão dos períodos com incidência de ruído ambiente. (LAeq tratado) ";
//    private String RuidoAmbiente = "No \\textbf{Gráfico \\ponto{" + this.ClassificacaoToStringAbrev(this.getClassificacao()) + "}} é apresentado o histórico no tempo do nível sonoro do \\textbf{Ponto \\ponto{" + this.ClassificacaoToStringAbrev(this.getClassificacao()) + "}} sem tratamento, não houve necessidade de tratamento estatístico porque o ruído representado durante a amostragem foi somente do ambiente.";
//    private String RuidoEmpreendimento = "No \\textbf{Gráfico \\ponto{" + this.ClassificacaoToStringAbrev(this.getClassificacao()) + "}} é apresentado o histórico no tempo do nível sonoro do \\textbf{Ponto \\ponto{" + this.ClassificacaoToStringAbrev(this.getClassificacao()) + "}} sem tratamento, não houve influência do ruído ambiente e por isso não foi necessário tratamento estatístico.";
    private boolean fullRuidoAmbiente = false;

//    private double globalLAeq;
//    private double globalLAeqMin;
//    private double globalLAeqMax;
    //
    private Calendar inicio;
    private Calendar fim;

    private String getDescCodes() {
        this.Chamada = "Na \\textbf{Figura REF_GRAFICO} é apresentado o histórico no tempo do nível sonoro do \\textbf{Ponto \\ponto{" + this.ClassificacaoToStringAbrev(this.getClassificacao()) + "}} sem tratamento. ";
        this.descCod1 = "Os trechos na cor vermelha, identificados como \\textbf{Código 1} representam os períodos com incidência de ruído gerado pela passagem de veículos de pequeno porte na rua próxima ao ponto de amostragem. ";
        this.descCod2 = "Os trechos na cor verde, identificados como \\textbf{Código 2} representam os períodos com incidência de ruído gerado pela passagem de veículos de grande porte na avenida próxima ao ponto de amostragem. ";
        this.descCod3 = "Os trechos na cor azul, identificados como \\textbf{Código 3} representam os períodos com incidência de ruído ambiente gerado ";
        this.Fechamento = "A \\textbf{Figura REF_GRAFICO} apresenta o histórico no tempo do nível sonoro após tratamento dos dados e exclusão dos períodos com incidência de ruído ambiente. (LAeq tratado) ";
        this.RuidoAmbiente = "Na \\textbf{Figura REF_GRAFICO} é apresentado o histórico no tempo do nível sonoro do \\textbf{Ponto \\ponto{" + this.ClassificacaoToStringAbrev(this.getClassificacao()) + "}}, não houve necessidade de tratamento estatístico porque o ruído representado durante a amostragem foi somente do ambiente.";
        this.RuidoEmpreendimento = "Na \\textbf{Figura REF_GRAFICO} é apresentado o histórico no tempo do nível sonoro do \\textbf{Ponto \\ponto{" + this.ClassificacaoToStringAbrev(this.getClassificacao()) + "}}, não houve influência do ruído ambiente e por isso não foi necessário tratamento estatístico.";

        Boolean r = false;
        Boolean g = false;
        Boolean b = false;
        Boolean n = false;

        for (Leitura leitura : leituras) {
            if (leitura.getCodigo() == Leitura.CODIGO_1) {
                r = true;
            } else if (leitura.getCodigo() == Leitura.CODIGO_2) {
                g = true;
            } else if (leitura.getCodigo() == Leitura.CODIGO_3) {
                b = true;
            } else if (leitura.getCodigo() == Leitura.RESIDUAL) {
                n = true;
            }
        }

        StringBuffer sb = new StringBuffer();

        if ((r == false) && (g == false) && (b == false) && (n == true)) {
            if (isFullRuidoAmbiente()) {
                sb.append(this.RuidoAmbiente);
            } else {
                sb.append(this.RuidoEmpreendimento);
            }
        } else {
            if (isFullRuidoAmbiente()) {
                sb.append(this.RuidoAmbiente);
            } else {
                sb.append(this.getChamada());
                if (r) {
                    sb.append(this.getDescCod1());
                }
                if (g) {
                    sb.append(this.getDescCod2());
                }
                if (b) {
                    sb.append(this.getDescCod3());
                }
                sb.append(this.getFechamento());
            }
        }

        sb.append(
                " \\\\*");

        this.setDescricao(sb.toString());
        return this.getDescricao();
    }

    public String getChamada() {
        return Chamada;
    }

    public void setChamada(String Chamada) {
        this.Chamada = Chamada;
    }

    public String getFechamento() {
        return Fechamento;
    }

    public void setFechamento(String Fechamento) {
        this.Fechamento = Fechamento;
    }

    public String getDescCod1() {
        return descCod1;
    }

    public void setDescCod1(String descCod1) {
        this.descCod1 = descCod1;
    }

    public String getDescCod2() {
        return descCod2;
    }

    public void setDescCod2(String descCod2) {
        this.descCod2 = descCod2;
    }

    public String getDescCod3() {
        return descCod3;
    }

    public void setDescCod3(String descCod3) {
        this.descCod3 = descCod3;
    }

    public Ponto() {
    }

    public int getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(int classificacao) {
        this.classificacao = classificacao;
    }

    public String toString() {
        return String.valueOf(getNumero());
    }

    public void loadWithCodes(File f) throws Exception {
        if (f.isDirectory()) {
            throw new Exception("Arquivo é um diretório.");
        }

        FileInputStream fis = new FileInputStream(f);
//        InputStreamReader isr = new InputStreamReader(fis, "ANSI");
        InputStreamReader isr = new InputStreamReader(fis, "ISO-8859-1");
        BufferedReader br = new BufferedReader(isr);

        String[] linhaSplitada = br.readLine().split("\t");
        if (!((linhaSplitada[0].equals("Arquivo")) || (linhaSplitada[0].equals("File")))) { // Primeira linha
            throw new Exception("Arquivo corrompido ou desconhecido.");
        }
        setNome(linhaSplitada[1]);
        linhaSplitada = br.readLine().split("\t"); // Segunda Linha
        linhaSplitada = br.readLine().split("\t"); // Terceira Linha
        linhaSplitada = br.readLine().split("\t"); // Quarta Linha
        linhaSplitada = br.readLine().split("\t"); // Quinta Linha

        if (!((linhaSplitada[0].equals("Unidade")) || (linhaSplitada[0].equals("Unit")))) { // Primeira linha
            throw new Exception("Arquivo corrompido ou desconhecido.");
        }

        linhaSplitada = br.readLine().split("\t"); // Sexta Linha
        linhaSplitada = br.readLine().split("\t"); // Sétima Linha

        setInicio(DateAdapter.StringToCalendar(linhaSplitada[1]));

        linhaSplitada = br.readLine().split("\t"); // Oitava Linha

        setFim(DateAdapter.StringToCalendar(linhaSplitada[1]));

        linhaSplitada = br.readLine().split("\t"); // Nona Linha

        String[] titulos = linhaSplitada;

        linhaSplitada = br.readLine().split("\t"); // Décima Linha

        ArrayList<Leitura> leituras = new ArrayList<Leitura>();
        linhaSplitada = br.readLine().split("\t");

        do {
            Leitura leitura = new Leitura();
            if ((titulos.length >= 2) && (!linhaSplitada[1].equals(""))) {
                leitura.setCodigo(getCode(titulos[1]));
                leitura.setHorario(DateAdapter.StringToCalendar(linhaSplitada[0]));
                leitura.setLAeq(Double.parseDouble(linhaSplitada[1].replace(",", ".")));
                leitura.setLmin(Double.parseDouble(linhaSplitada[2].replace(",", ".")));
                leitura.setLmax(Double.parseDouble(linhaSplitada[3].replace(",", ".")));

                int horas = Integer.parseInt(linhaSplitada[4].split(":")[0]);
                int minutos = Integer.parseInt(linhaSplitada[4].split(":")[1]);
                int segundos = Integer.parseInt(linhaSplitada[4].split(":")[2]);

                leitura.setDuracao((((horas) * 60 + minutos) * 60 + segundos));
            } else if ((titulos.length >= 3) && (!linhaSplitada[5].equals(""))) {
                leitura.setCodigo(getCode(titulos[2]));
                leitura.setHorario(DateAdapter.StringToCalendar(linhaSplitada[0]));
                leitura.setLAeq(Double.parseDouble(linhaSplitada[5].replace(",", ".")));
                leitura.setLmin(Double.parseDouble(linhaSplitada[6].replace(",", ".")));
                leitura.setLmax(Double.parseDouble(linhaSplitada[7].replace(",", ".")));

                int horas = Integer.parseInt(linhaSplitada[8].split(":")[0]);
                int minutos = Integer.parseInt(linhaSplitada[8].split(":")[1]);
                int segundos = Integer.parseInt(linhaSplitada[8].split(":")[2]);

                leitura.setDuracao((((horas) * 60 + minutos) * 60 + segundos));
            } else if ((titulos.length >= 4) && (!linhaSplitada[9].equals(""))) {
                leitura.setCodigo(getCode(titulos[3]));
                leitura.setHorario(DateAdapter.StringToCalendar(linhaSplitada[0]));
                leitura.setLAeq(Double.parseDouble(linhaSplitada[9].replace(",", ".")));
                leitura.setLmin(Double.parseDouble(linhaSplitada[10].replace(",", ".")));
                leitura.setLmax(Double.parseDouble(linhaSplitada[11].replace(",", ".")));

                int horas = Integer.parseInt(linhaSplitada[12].split(":")[0]);
                int minutos = Integer.parseInt(linhaSplitada[12].split(":")[1]);
                int segundos = Integer.parseInt(linhaSplitada[12].split(":")[2]);

                leitura.setDuracao((((horas) * 60 + minutos) * 60 + segundos));
            } else if ((titulos.length >= 5) && (!linhaSplitada[13].equals(""))) {
                leitura.setCodigo(getCode(titulos[4]));
                leitura.setHorario(DateAdapter.StringToCalendar(linhaSplitada[0]));
                leitura.setLAeq(Double.parseDouble(linhaSplitada[13].replace(",", ".")));
                leitura.setLmin(Double.parseDouble(linhaSplitada[14].replace(",", ".")));
                leitura.setLmax(Double.parseDouble(linhaSplitada[15].replace(",", ".")));

                int horas = Integer.parseInt(linhaSplitada[16].split(":")[0]);
                int minutos = Integer.parseInt(linhaSplitada[16].split(":")[1]);
                int segundos = Integer.parseInt(linhaSplitada[16].split(":")[2]);

                leitura.setDuracao((((horas) * 60 + minutos) * 60 + segundos));
            }
            leituras.add(leitura);
            linhaSplitada = br.readLine().split("\t");
//        } while (!linhaSplitada[0].equals("Global "));
        } while (!((linhaSplitada[0].equals("Global ")) || (linhaSplitada[0].equals("Overall "))));

        this.leituras = leituras.toArray(new Leitura[leituras.size()]);
        br.close();

    }

    public void load(File f) throws Exception {
        if (f.isDirectory()) {
            throw new Exception("Arquivo Fé um diretório.");
        }

        FileInputStream fis = new FileInputStream(f);
//        InputStreamReader isr = new InputStreamReader(fis, "ANSI");
        InputStreamReader isr = new InputStreamReader(fis, "ISO-8859-1");
        BufferedReader br = new BufferedReader(isr);

        String[] linhaLida = br.readLine().split("\t");
//        if (!linhaLida[0].equals("Arquivo")) { // Primeira linha
        if (!((linhaLida[0].equals("Arquivo")) || (linhaLida[0].equals("File")))) { // Primeira linha
            throw new Exception("Arquivo corrompido ou desconhecido.");
        }
        setNome(linhaLida[1]);
        linhaLida = br.readLine().split("\t"); // Segunda Linha

        int duracao = Integer.parseInt(linhaLida[1].replace("s", ""));

        linhaLida = br.readLine().split("\t"); // Terceira Linha

        setInicio(DateAdapter.StringToCalendar(linhaLida[1]));

        linhaLida = br.readLine().split("\t"); // Quarta Linha

        setFim(DateAdapter.StringToCalendar(linhaLida[1]));

        linhaLida = br.readLine().split("\t"); // Quinta Linha

//        if (!linhaLida[0].equals("Localização")) {
        if (!((linhaLida[0].equals("Localização")) || (linhaLida[0].equals("Location")))) {
            throw new Exception("Arquivo corrompido ou desconhecido.");
        }

        linhaLida = br.readLine().split("\t"); // Sexta Linha
        linhaLida = br.readLine().split("\t"); // Sétima Linha
        linhaLida = br.readLine().split("\t"); // Oitava Linha
        linhaLida = br.readLine().split("\t"); // Nona Linha

        ArrayList<Leitura> leituras = new ArrayList<Leitura>();

        linhaLida = br.readLine().split("\t"); // Décima Linha
        do {
            Leitura leitura = new Leitura();

            leitura.setHorario(DateAdapter.StringToCalendar(linhaLida[0]));
            leitura.setCodigo(Leitura.RESIDUAL);
            leitura.setLAeq(Double.parseDouble(linhaLida[1].replace(",", ".")));
            leitura.setLmin(Double.parseDouble(linhaLida[2].replace(",", ".")));
            leitura.setLmax(Double.parseDouble(linhaLida[3].replace(",", ".")));

            leitura.setDuracao(duracao);

            leituras.add(leitura);
            linhaLida = br.readLine().split("\t"); // Nona Linha
        } while (!((linhaLida[0].equals("Global ")) || (linhaLida[0].equals("Overall "))));
//        } while (!linhaLida[0].equals("Global "));

        this.leituras = leituras.toArray(new Leitura[leituras.size()]);
        br.close();
        fis.close();
    }

    private int getCode(String code) {
        if (code.equals("Código 1")) {
            return Leitura.CODIGO_1;
        } else if (code.equals("Código 2")) {
            return Leitura.CODIGO_2;
        } else if (code.equals("Código 3")) {
            return Leitura.CODIGO_3;
        } else if (code.equals("Residual")) {
            return Leitura.RESIDUAL;
        } else { // (code.equals("Fontes listadas juntas"))
            return -1;
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getDescricao() {
        if (this.descricao != null) {
            return this.descricao;
        }
        return getDescCodes(); // engine dos códigos.
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Leitura[] getLeituras() {
        return leituras;
    }

    public void setLeituras(Leitura[] leituras) {
        this.leituras = leituras;
    }

    public Foto getFoto() {
        return foto;
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }

    public Calendar getInicio() {
        return inicio;
    }

    public void setInicio(Calendar inicio) {
        this.inicio = inicio;
    }

    public Calendar getFim() {
        return fim;
    }

    public void setFim(Calendar fim) {
        this.fim = fim;
    }

    public Leitura[] getLeituras(int codigo) {
        ArrayList<Leitura> l = new ArrayList<Leitura>();
        if (codigo != Leitura.AMBIENTAL) { // verifica se o código não é "ambiental", que deverá executar o for abaixo do else.
            for (Leitura leitura : this.leituras) {
                if (leitura.getCodigo() == codigo) {
                    l.add(leitura);
                }
            }
        } else { // se for igual ao ambiental, pegará todos menos o residual;
            for (Leitura leitura : this.leituras) {
                if (leitura.getCodigo() != Leitura.RESIDUAL) {
                    l.add(leitura);
                }
            }
        }
        return l.toArray(new Leitura[l.size()]);
    }

    public double getGlobalLAeq() {
        return LAeq.CalcLAeq(this.leituras);
    }

//    public double getResidualLAeq() {
//        return LAeq.CalcLAeq(this.getLeituras(Leitura.RESIDUAL));
//    }
    public double getLAeqMin() {
        Leitura[] leituras = this.getLeituras();
        double retorno = 10000.0;
        for (Leitura leitura : leituras) {
            if (retorno > leitura.getLAeq()) {
                retorno = leitura.getLAeq();
            }
        }
        return retorno;
    }

    public double getLAeqMax() {
        Leitura[] leituras = this.getLeituras();
        double retorno = 0.0;
        for (Leitura leitura : leituras) {
            if (retorno < leitura.getLAeq()) {
                retorno = leitura.getLAeq();
            }
        }
        return retorno;
    }

    public double getLAeqArredondado() {
        double LAeq = Math.round(this.getLAeqTratado());
        return LAeq;
    }

    public double getLAeqTratado() {
        if (this.getLeituras(Leitura.RESIDUAL).length == 0) {
            return 0.00;
        }

        return LAeq.CalcLAeq(this.getLeituras(Leitura.RESIDUAL));
    }

    public double getLra() {
        if (this.isFullRuidoAmbiente() != true) {
            return 0.0;
        } else {
            return getGlobalLAeq();
        }
    }

    public static String ClassificacaoToString(int classificacao) {
        return Ponto.decodificarClassificacao[classificacao];
    }

    public static String ClassificacaoToStringAbrev(int classificacao) {
        return Ponto.decodificarClassificacaoAbreviada[classificacao];
    }

    @Override
    public int compareTo(Ponto o) {
        if (getNumero() < o.getNumero()) {
            return -1;
        } else if (getNumero() == o.getNumero()) {
            return 0;
        } else {
            return 1;
        }
    }

    private int iNCA = NCA.AREA_PREDOMINANTEMENTE_INDUSTRIAL;
    private NCA nca;

    public NCA getNCA() {
        if (nca == null) {
            this.nca = new NCA(iNCA);
        }

        return this.nca;
    }

    public int getiNCA() {
        return iNCA;
    }

    public void setiNCA(int iNCA) {
        this.iNCA = iNCA;
    }

    public void setNca(NCA nca) {
        this.nca = nca;
    }

    public String getComparacao() {
        NCA nca = this.getNCA();
        switch (this.classificacao) {
            case Ponto.TARDE:
                return nca.compareLraWithTardeString(this);
            case Ponto.NOITE:
                return nca.compareLraWithNoiteString(this);
            default:
                return nca.compareLraWithManhaString(this);
        }
    }

    public boolean isFullRuidoAmbiente() {
        if (getLeituras(Leitura.RESIDUAL).length == 0) {
            return true;
        }
        return false;
    }

    public boolean isFullResidual() {
        if (getLeituras(Leitura.RESIDUAL).length == 180) {
            return true;
        }
        return false;
    }

//    public void setFullRuidoAmbiente(boolean fullRuidoAmbiente) {
//        this.fullRuidoAmbiente = fullRuidoAmbiente;
//    }
}
