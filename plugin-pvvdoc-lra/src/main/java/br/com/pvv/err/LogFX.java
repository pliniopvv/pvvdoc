/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.err;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

/**
 *
 * @author PLINIO
 */
public class LogFX {

    public static void alert(Exception e) {
        try {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintWriter pw = new PrintWriter(baos);
            e.printStackTrace(pw);
            pw.flush();
            buildDialog(e.getMessage(), baos.toByteArray());
        } catch (Exception ex) {
            new LogFrame().setException(e);
        }
    }

    public static void buildDialog(String msg, byte[] bytes) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Uma situação inesperada ocorreu!");
        alert.setHeaderText("Atenção, uma situação inesperada ocorreu!\n"
                + "favor informar ao desenvolvedor o que ocorreu, juntamente com o conteúdo abaixo.");
//        alert.setHeaderText("Falha na aplicação! ");
        alert.setContentText(msg);
        TextArea ta = new TextArea(new String(bytes));
        ta.setMaxWidth(Double.MAX_VALUE);
        ta.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(ta, Priority.ALWAYS);
        GridPane.setHgrow(ta, Priority.ALWAYS);
//        ta.setWrapText(true);
        Label l = new Label("StackTrace Exception:");

        GridPane grid = new GridPane();
        grid.setMaxWidth(Double.MAX_VALUE);
        grid.add(l, 0, 0);
        grid.add(ta, 0, 1);

        alert.getDialogPane().setExpandableContent(grid);
//        alert.getDialogPane().setExpandableContent(l);
//        alert.getDialogPane().setExpandableContent(ta);

        try {
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            LogFX w = new LogFX();
            stage.getIcons().add(new Image(w.getClass().getResourceAsStream("/br/com/pvv/imgs/logo.png")));
        } catch (Exception e) {
            LogFX.alert(e);
        }

        alert.showAndWait();
    }

}
