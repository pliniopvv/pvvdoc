/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.form.properties;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author Plinio
 */
public class PropertiesFormStart extends Application {

    private Stage stage;
    private Properties properties;
    //
    private Map<String, TextField> listaCampos = new HashMap<>();

    public PropertiesFormStart(Properties properties) {
        this.properties = properties;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public Map<String, TextField> getListaCampos() {
        return listaCampos;
    }

    public void setListaCampos(Map<String, TextField> listaCampos) {
        this.listaCampos = listaCampos;
    }

    @Override
    public void start(Stage stage) {
        this.stage = stage;
        // Parent root = FXMLLoader.load(getClass().getResource("/fxml/PontoEditor.fxml"));
        //  Scene scene = new Scene(root);

        VBox v = new VBox();
        v.setFillWidth(true);
        ScrollPane sp = new ScrollPane(v);
//        sp.setMaxWidth(800.0);
        sp.setMaxWidth(Double.MAX_VALUE);
        Set<Object> ks = properties.keySet();
        for (Object o : ks) {
            v.getChildren().add(createPropertieBox(o.toString()));
        }

        Scene scene = new Scene(sp);
//        Scene scene = new Scene(sp, 800, 600);
        stage.setTitle("Editor de pontos.");
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/br/com/pvv/imgs/logo.png")));
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setOnCloseRequest((WindowEvent event) -> {
//            System.exit(0);
        });
        stage.show();
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    private Node createPropertieBox(String label) {
        VBox v = new VBox();
        v.setFillWidth(true);
        v.setPrefWidth(600);
//        v.setMaxWidth(Double.MAX_VALUE);
        Label l = new Label(label);
        VBox.setVgrow(l, Priority.ALWAYS);
        l.setMaxWidth(Double.MAX_VALUE);
        TextField tf = new TextField();
        tf.setMaxWidth(Double.MAX_VALUE);
//        tf.setPrefWidth(Double.MAX_VALUE);
        v.getChildren().add(l);
        v.getChildren().add(tf);
        getListaCampos().put(label, tf);
        return v;
    }

}
