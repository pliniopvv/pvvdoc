/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STVerticalJc;

/**
 *
 * @author Plinio
 */
public class PaginaPontoBuilder {

//    private XWPFDocument documento;
    private XWPFParagraph paragrafo;

    private void colocarBorda(XWPFTableCell cell) {
        CTTc ctTc = cell.getCTTc();
        // here is need to change... 
        CTTcPr tcPr = ctTc.addNewTcPr();
        CTTcBorders border = tcPr.addNewTcBorders();

        border.addNewBottom().setVal(STBorder.NONE);
        border.addNewRight().setVal(STBorder.NONE);
        border.addNewLeft().setVal(STBorder.NONE);
        border.addNewTop().setVal(STBorder.NONE);
    }

    public enum PERIODO {
        MANHA("M", "Manhã"), TARDE("T", "Tarde"), NOITE("N", "Noite");

        private String p;
        private String g;

        private PERIODO(String p, String g) {
            this.p = p;
            this.g = g;
        }

        public String getP() {
            return p;
        }

        public void setP(String p) {
            this.p = p;
        }

        public String getG() {
            return g;
        }

        public void setG(String g) {
            this.g = g;
        }

    }

    public PaginaPontoBuilder(XWPFParagraph paragrafo) {
//    public PaginaPontoBuilder(XWPFDocument documento) {
//        this.documento = documento;
        this.paragrafo = paragrafo;
    }

    public PaginaPontoBuilder addTitle(PERIODO periodo) {
//        XWPFParagraph p = documento.createParagraph();
        XmlCursor cursor = paragrafo.getCTP().newCursor();
        XWPFParagraph p = paragrafo.getDocument().insertNewParagraph(cursor);
        p.setStyle("TabelaDePontos");

        XWPFRun r = p.createRun();
        r.setText("Tabela " + periodo.getP());
        p.getCTP().addNewFldSimple().setInstr("SEQ p" + periodo.getP());
        r = p.createRun();
        r.setText(" – Resultados do Ponto ");
        p.getCTP().addNewFldSimple().setInstr("SEQ Ponto-" + periodo.getP());
        r = p.createRun();
        r.setText(" – Período da " + periodo.getG() + " – Externo.");
        r.addCarriageReturn();
        return this;
    }

    public PaginaPontoBuilder addTitle(String titulo) {
//        XWPFParagraph p = documento.createParagraph();
        XmlCursor cursor = paragrafo.getCTP().newCursor();
        XWPFParagraph p = paragrafo.getDocument().insertNewParagraph(cursor);
        XWPFRun r = p.createRun();
        p.setStyle("TabelaDePontos");
        r.setText(titulo);
        r.addCarriageReturn();
        return this;
    }

    public PaginaPontoBuilder addTableInfo(String nomeDoArquivo, String inicioDaColeta, String fimDaColeta, double Leq, double Lmin, double Lmax, double LAeq, double LAeqArr) {
//        XWPFDocument document = documento;
//
//        //create table
//        XWPFTable table = document.createTable();
        XmlCursor cursor = paragrafo.getCTP().newCursor();
        XWPFTable table = paragrafo.getDocument().insertNewTbl(cursor);

        /*
        PRIMEIRA LINHA
         */
        XWPFTableRow tableRow1 = table.getRow(0);
        XWPFTableCell cell = tableRow1.getCell(0);
        cell.setColor("A5A5A5");
        //SETANDO NEGRITO
        XWPFRun r = cell.getParagraphArray(0).insertNewRun(0);
        r.setBold(true);
        r.setText("File");
        //
        cell = tableRow1.addNewTableCell();
        cell.setText(nomeDoArquivo);
        cell.getCTTc().addNewTcPr();
        cell.getCTTc().getTcPr().addNewGridSpan();
        cell.getCTTc().getTcPr().getGridSpan().setVal(BigInteger.valueOf(2L));

        cell = tableRow1.addNewTableCell();
        removeBorder(cell);
        cell = tableRow1.addNewTableCell();
        removeBorder(cell);
        cell = tableRow1.addNewTableCell();
        removeBorder(cell);
        cell = tableRow1.addNewTableCell();
        removeBorder(cell);
        cell = tableRow1.addNewTableCell();
        removeBorder(cell);
        cell = tableRow1.addNewTableCell();
        removeBorder(cell);
        cell = tableRow1.addNewTableCell();
        removeBorder(cell);

        /*
        SEGUNDA LINHA
         */
        XWPFTableRow tableRow2 = table.createRow();
        cell = tableRow2.getCell(0);
        cell.setColor("A5A5A5");
        //SETANDO NEGRITO
        r = cell.getParagraphArray(0).insertNewRun(0);
        r.setBold(true);
        r.setText("Start");
        //
        cell = tableRow2.getCell(1);
        cell.setText(inicioDaColeta);
        cell.getCTTc().addNewTcPr();
        cell.getCTTc().getTcPr().addNewGridSpan();
        cell.getCTTc().getTcPr().getGridSpan().setVal(BigInteger.valueOf(2L));

        cell = tableRow2.getCell(2);
        removeBorder(cell);
        cell = tableRow2.getCell(3);
        removeBorder(cell);
        cell = tableRow2.getCell(4);
        removeBorder(cell);
        cell = tableRow2.getCell(5);
        removeBorder(cell);
        cell = tableRow2.getCell(6);
        removeBorder(cell);
        cell = tableRow2.getCell(7);
        removeBorder(cell);
        cell = tableRow2.getCell(8);
        removeBorder(cell);

        /*
        TERCEIRA LINHA
         */
        XWPFTableRow tableRow3 = table.createRow();
        cell = tableRow3.getCell(0);
        cell.setColor("A5A5A5");
        //SETANDO NEGRITO
        r = cell.getParagraphArray(0).insertNewRun(0);
        r.setBold(true);
        r.setText("End");
        //
        cell = tableRow3.getCell(1);
        cell.setText(fimDaColeta);
        cell.getCTTc().addNewTcPr();
        cell.getCTTc().getTcPr().addNewGridSpan();
        cell.getCTTc().getTcPr().getGridSpan().setVal(BigInteger.valueOf(2L));

        cell = tableRow3.getCell(2);
        removeBorder(cell);
        cell = tableRow3.getCell(3);
        removeBorder(cell);
        cell = tableRow3.getCell(4);
        removeBorder(cell);
        cell = tableRow3.getCell(5);
        removeBorder(cell);
        cell = tableRow3.getCell(6);
        removeBorder(cell);
        cell = tableRow3.getCell(7);
        removeBorder(cell);
        cell = tableRow3.getCell(8);
        removeBorder(cell);

        /*
        QUARTA LINHA
         */
        XWPFTableRow tableRow4 = table.createRow();
        tableRow4.setHeight(104);
        setConfigCell(tableRow4, 0, true, "Channel");

        cell = tableRow4.getCell(1);
        colocarBorda(cell);
        setConfigCell(tableRow4, 1, true, "Type");

        cell = tableRow4.getCell(2);
        setConfigCell(tableRow4, 2, true, "Wght");

        cell = tableRow4.getCell(3);
        setConfigCell(tableRow4, 3, true, "Unit");
        cell.setWidth("563");

        cell = tableRow4.getCell(4);
        setConfigCell(tableRow4, 4, true, "Leq");
        cell.setWidth("563");

        cell = tableRow4.getCell(5);
        setConfigCell(tableRow4, 5, true, "Lmin");
        cell.setWidth("563");

        cell = tableRow4.getCell(5);
        setConfigCell(tableRow4, 6, true, "Lmax");
        cell.setWidth("563");

        cell = tableRow4.getCell(6);
        setConfigCell(tableRow4, 7, true, "LAeq tratado");
        cell.setWidth("563");

        cell = tableRow4.getCell(7);
        setConfigCell(tableRow4, 8, true, "LAeq tratado Arredondamento conforme      NBR10151/2000 (valor considerado)");
        cell.setWidth("2253");

        /*
        QUINTA LINHA
         */
        XWPFTableRow tableRow5 = table.createRow();
        tableRow4.setHeight(104);
        setConfigCell(tableRow5, 0, false, "#5016");

        cell = tableRow4.getCell(1);
        setConfigCell(tableRow5, 1, false, "Leq");

        cell = tableRow4.getCell(2);
        setConfigCell(tableRow5, 2, false, "A");

        cell = tableRow4.getCell(3);
        setConfigCell(tableRow5, 3, false, "dB");
        cell.setWidth("563");

        cell = tableRow4.getCell(4);
        setConfigCell(tableRow5, 4, false, String.valueOf(Leq));
        cell.setWidth("563");

        cell = tableRow4.getCell(5);
        setConfigCell(tableRow5, 5, false, String.valueOf(Lmin));
        cell.setWidth("563");

        cell = tableRow4.getCell(6);
        setConfigCell(tableRow5, 6, false, String.valueOf(Lmax));
        cell.setWidth("563");

        cell = tableRow4.getCell(7);
        setConfigCell(tableRow5, 7, false, String.valueOf(LAeq));
        cell.setWidth("563");

        cell = tableRow4.getCell(8);
        setConfigCell(tableRow5, 8, false, String.valueOf(LAeqArr));
        cell.setWidth("2253");

        cursor = paragrafo.getCTP().newCursor();
        paragrafo.getDocument().insertNewParagraph(cursor);
        return this;
    }

    private void removeBorder(XWPFTableCell cell) {
        CTTc ctTc = cell.getCTTc();
        // here is need to change... 
        CTTcPr tcPr = ctTc.addNewTcPr();
        CTTcBorders border = tcPr.addNewTcBorders();

        border.addNewBottom().setVal(STBorder.NIL);
        border.addNewRight().setVal(STBorder.NIL);
        border.addNewLeft().setVal(STBorder.NIL);
        border.addNewTop().setVal(STBorder.NIL);

    }

    private void setBoldText(XWPFTableCell cell, String text) {
        //SETANDO NEGRITO
        XWPFRun r = cell.getParagraphArray(0).insertNewRun(0);
        r.setBold(true);
        r.setText(text);
    }

    private void setConfigCell(XWPFTableRow tableRow, int i, boolean b, String text) {
        XWPFTableCell cell = tableRow.getCell(i);
        if (b) {
            cell.setColor("A5A5A5");
            setBoldText(cell, text);
        } else {
            cell.setText(text);
        }
        cell.getCTTc().addNewTcPr().addNewVAlign().setVal(STVerticalJc.CENTER);
        cell.getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
    }

    public PaginaPontoBuilder addDescription(String description) {
//        XWPFParagraph p = documento.createParagraph();
        XmlCursor cursor = paragrafo.getCTP().newCursor();
        XWPFParagraph p = paragrafo.getDocument().insertNewParagraph(cursor);
//        XWPFParagraph p = paragrafo;
        p.setAlignment(ParagraphAlignment.BOTH);
        p.setStyle("NormalDescription");
        XWPFRun r = p.createRun();
        r.setText(description);
        return this;
    }

    public PaginaPontoBuilder addOneGrafico(File imgFile, String caption) throws InvalidFormatException, FileNotFoundException, IOException {
        /*
            INCLUINDO IMAGENS
         */
        FileInputStream fis = new FileInputStream(imgFile);
//        XWPFParagraph p = documento.createParagraph();
        XmlCursor cursor = paragrafo.getCTP().newCursor();
        XWPFParagraph p = paragrafo.getDocument().insertNewParagraph(cursor);
        p.createRun().addPicture(fis, Document.PICTURE_TYPE_PNG, imgFile.getName(), Units.toEMU(382.6530612244898), Units.toEMU(276.0770975056689)); // 28,34467120181406 emu - 1 cm
        p.setAlignment(ParagraphAlignment.CENTER);
        fis.close();

//        p = documento.createParagraph();
        cursor = paragrafo.getCTP().newCursor();
        p = paragrafo.getDocument().insertNewParagraph(cursor);
        p.setStyle("Graficos");
        p.createRun().setText(caption);
        return this;
    }

    public PaginaPontoBuilder addOneGrafico(InputStream isFile, String caption) throws InvalidFormatException, FileNotFoundException, IOException {
        /*
            INCLUINDO IMAGENS
         */
//        XWPFParagraph p = documento.createParagraph();
        XmlCursor cursor = paragrafo.getCTP().newCursor();
        XWPFParagraph p = paragrafo.getDocument().insertNewParagraph(cursor);
        p.createRun().addPicture(isFile, Document.PICTURE_TYPE_PNG, "Grafico Unico", Units.toEMU(382.6530612244898), Units.toEMU(276.0770975056689)); // 28,34467120181406 emu - 1 cm
        p.setAlignment(ParagraphAlignment.CENTER);
        isFile.close();

//        p = documento.createParagraph();
        cursor = paragrafo.getCTP().newCursor();
        p = paragrafo.getDocument().insertNewParagraph(cursor);
        p.setStyle("Graficos");
        p.createRun().setText(caption);
        return this;
    }

    public PaginaPontoBuilder addTwoGraficos(File imgGrafico1, String caption1, File imgGrafico2, String caption2) throws FileNotFoundException, InvalidFormatException, IOException {
        /*
            INCLUINDO PRIMEIRA IMAGEM
         */
//        XWPFTable table = documento.createTable();
        XmlCursor cursor = paragrafo.getCTP().newCursor();
        XWPFTable table = paragrafo.getDocument().insertNewTbl(cursor);

        CTTblPr tblPr = table.getCTTbl().getTblPr();
        CTJc jc = (tblPr.isSetJc() ? tblPr.getJc() : tblPr.addNewJc());
        jc.setVal(STJc.CENTER);

        XWPFTableRow row = table.getRow(0);
        XWPFTableCell cell1 = row.getCell(0);
        XWPFTableCell cell2 = row.createCell();
        removeBorder(cell1);
        removeBorder(cell2);

        XWPFParagraph p = cell1.addParagraph();
        FileInputStream fis = new FileInputStream(imgGrafico1);
        p.createRun().addPicture(fis, Document.PICTURE_TYPE_PNG, imgGrafico1.getName(), Units.toEMU(226.7573696145125), Units.toEMU(163.5487528344671)); // 28,34467120181406 emu - 1 cm
        fis.close();
        p.setAlignment(ParagraphAlignment.CENTER);
        p = cell1.addParagraph();
        p.setStyle("Graficos");
        p.createRun().setText(caption1);

        p = cell2.addParagraph();
        fis = new FileInputStream(imgGrafico2);
        p.createRun().addPicture(fis, Document.PICTURE_TYPE_PNG, imgGrafico2.getName(), Units.toEMU(226.7573696145125), Units.toEMU(163.5487528344671)); // 28,34467120181406 emu - 1 cm
        fis.close();
        p.setAlignment(ParagraphAlignment.CENTER);
        p = cell2.addParagraph();
        p.setStyle("Graficos");
        p.createRun().setText(caption2);
        return this;
    }

    public PaginaPontoBuilder addTwoGraficos(InputStream isGrafico1, String caption1, InputStream isGrafico2, String caption2) throws FileNotFoundException, InvalidFormatException, IOException {
        /*
            INCLUINDO PRIMEIRA IMAGEM
         */
//        XWPFTable table = documento.createTable();
        XmlCursor cursor = paragrafo.getCTP().newCursor();
        XWPFTable table = paragrafo.getDocument().insertNewTbl(cursor);

        CTTblPr tblPr = table.getCTTbl().getTblPr();
        CTJc jc = (tblPr.isSetJc() ? tblPr.getJc() : tblPr.addNewJc());
        jc.setVal(STJc.CENTER);

        XWPFTableRow row = table.getRow(0);
        XWPFTableCell cell1 = row.getCell(0);
        XWPFTableCell cell2 = row.createCell();
        removeBorder(cell1);
        removeBorder(cell2);

        XWPFParagraph p = cell1.addParagraph();
        p.createRun().addPicture(isGrafico1, Document.PICTURE_TYPE_PNG, "Figura 2", Units.toEMU(226.7573696145125), Units.toEMU(163.5487528344671)); // 28,34467120181406 emu - 1 cm
        isGrafico1.close();
        p.setAlignment(ParagraphAlignment.CENTER);
        p = cell1.addParagraph();
        p.setStyle("Graficos");
        p.createRun().setText(caption1);

        p = cell2.addParagraph();
        p.createRun().addPicture(isGrafico2, Document.PICTURE_TYPE_PNG, "Figura 2", Units.toEMU(226.7573696145125), Units.toEMU(163.5487528344671)); // 28,34467120181406 emu - 1 cm
        isGrafico2.close();
        p.setAlignment(ParagraphAlignment.CENTER);
        p = cell2.addParagraph();
        p.setStyle("Graficos");
        p.createRun().setText(caption2);
        return this;
    }

}
