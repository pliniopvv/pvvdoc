/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

/**
 *
 * @author Plinio
 */
public class VariaveisScanner {

    private XWPFDocument document;

    public VariaveisScanner(XWPFDocument document) {
        this.document = document;
    }

    public Set<String> collect() {
        List<XWPFParagraph> paragrafos = document.getParagraphs();

        Set<String> listaVariaveis = new HashSet<String>();
        for (XWPFParagraph paragrafo : paragrafos) {
            StringScanner scan = new StringScanner(paragrafo.getText());
            List<String> lv = scan.collectWith("[", "]");
            listaVariaveis.addAll(lv);
        }
        return listaVariaveis;
    }

    public XWPFParagraph getParagrafo(String variavel) {
        List<XWPFParagraph> paragrafos = document.getParagraphs();
        Set<String> listaVariaveis = new HashSet<String>();
        for (XWPFParagraph paragrafo : paragrafos) {
            if (paragrafo.getText().contains(variavel)) {
                return paragrafo;
            }
        }
        return null;
    }

    public int getIndex(String variavel) {
        List<XWPFParagraph> paragrafos = document.getParagraphs();
        return paragrafos.indexOf(getParagrafo(variavel));
    }
}
