/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;

/**
 *
 * @author Plinio
 */
public class PaginaFotoBuilder {

    private XWPFDocument documento;
//    private XWPFParagraph paragrafo;

//    public PaginaFotoBuilder(XWPFParagraph paragraph) {
    public PaginaFotoBuilder(XWPFDocument documento) {
//        this.paragrafo = paragrafo;
    }

    public PaginaFotoBuilder addTresFotos(XWPFParagraph paragrafo, File foto1, String caption1, File foto2, String caption2, File foto3, String caption3) throws IOException, InvalidFormatException {
        /*
            INCLUINDO PRIMEIRA IMAGEM
         */
//        XWPFTable table = documento.createTable();
        XmlCursor cursor = paragrafo.getCTP().newCursor();
        XWPFTable table = documento.insertNewTbl(cursor);

        CTTblPr tblPr = table.getCTTbl().getTblPr();
        CTJc jc = (tblPr.isSetJc() ? tblPr.getJc() : tblPr.addNewJc());
        jc.setVal(STJc.CENTER);

        XWPFTableRow row = table.getRow(0);
        XWPFTableCell cell1 = row.getCell(0);
        XWPFTableCell cell2 = row.createCell();
        XWPFTableCell cell3 = row.createCell();
        removeBorder(cell1);
        removeBorder(cell2);
        removeBorder(cell3);

        XWPFParagraph p = cell1.addParagraph();
        FileInputStream fis = new FileInputStream(foto1);
        p.createRun().addPicture(fis, Document.PICTURE_TYPE_PNG, foto1.getName(), Units.toEMU(152.7777777777778), Units.toEMU(203.514739229025)); // 28,34467120181406 emu - 1 cm
        fis.close();
        p.setAlignment(ParagraphAlignment.CENTER);
        p = cell1.addParagraph();
        p.setStyle("Figuras");
        p.createRun().setText(caption1);

        p = cell2.addParagraph();
        fis = new FileInputStream(foto2);
        p.createRun().addPicture(fis, Document.PICTURE_TYPE_PNG, foto2.getName(), Units.toEMU(152.7777777777778), Units.toEMU(203.514739229025)); // 28,34467120181406 emu - 1 cm
        fis.close();
        p.setAlignment(ParagraphAlignment.CENTER);
        p = cell2.addParagraph();
        p.setStyle("Figuras");
        p.createRun().setText(caption2);

        p = cell3.addParagraph();
        fis = new FileInputStream(foto3);
        p.createRun().addPicture(fis, Document.PICTURE_TYPE_PNG, foto3.getName(), Units.toEMU(152.7777777777778), Units.toEMU(203.514739229025)); // 28,34467120181406 emu - 1 cm
        fis.close();
        p.setAlignment(ParagraphAlignment.CENTER);
        p = cell3.addParagraph();
        p.setStyle("Figuras");
        p.createRun().setText(caption3);
        return this;
    }

    private void removeBorder(XWPFTableCell cell) {
        CTTc ctTc = cell.getCTTc();
        // here is need to change... 
        CTTcPr tcPr = ctTc.addNewTcPr();
        CTTcBorders border = tcPr.addNewTcBorders();

        border.addNewBottom().setVal(STBorder.NIL);
        border.addNewRight().setVal(STBorder.NIL);
        border.addNewLeft().setVal(STBorder.NIL);
        border.addNewTop().setVal(STBorder.NIL);

    }
}
