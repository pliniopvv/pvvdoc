/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Plinio
 */
public class StringScanner {

    private String linha;

    public StringScanner(String linha) {
        this.linha = linha;
    }

    public List<String> collect(String stringInicio, String stringFim) {
        ArrayList<String> parametros = new ArrayList<>();
        String subs = linha;
        do {
            int i = subs.indexOf(stringInicio);
            if (i == -1) {
                break;
            }

            subs = subs.substring(i + stringInicio.length());
            int f = subs.indexOf(stringFim);
            try {
//                System.out.println(subs);
                parametros.add(subs.substring(0, f));
            } catch (Exception e) {
                System.out.println(subs);
                e.printStackTrace();
            }
        } while (subs.contains(stringInicio));
        return parametros;
    }

    public List<String> collectWith(String stringInicio, String stringFim) {
        ArrayList<String> parametros = new ArrayList<>();
        String subs = linha;
        do {
            int i = subs.indexOf(stringInicio);
            if (i == -1) {
                break;
            }

            subs = subs.substring(i + stringInicio.length());
            int f = subs.indexOf(stringFim);
            try {
//                System.out.println(subs);
                parametros.add(stringInicio + subs.substring(0, f) + stringFim);
            } catch (Exception e) {
                System.out.println(subs);
                e.printStackTrace();
            }
        } while (subs.contains(stringInicio));
        return parametros;
    }

}
