/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.doc;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.poi.xwpf.usermodel.PositionInParagraph;
import org.apache.poi.xwpf.usermodel.TextSegment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

/**
 *
 * @author Plinio
 */
public class VariavelReplacer {

    private XWPFDocument documento;

    public VariavelReplacer(XWPFDocument documento) {
        this.documento = documento;
    }

//    public VariavelReplacer replace(Properties propriedades) {
//        VariaveisScanner vs = new VariaveisScanner(documento);
//        for (Object o : propriedades.keySet()) {
//            String v = (String) o;
//            XWPFParagraph p;
//            while ((p = vs.getParagrafo(v)) != null) {
////                p.
//            }
//        }
//
//        return this;
//    }
    public Set<XWPFRun> getRunsWithVar(XWPFParagraph paragrafo, String variavel) {
        char[] c = variavel.toCharArray();
        Set<XWPFRun> lr = new HashSet<XWPFRun>();

        boolean found = false;
        int cont = 0;
        for (XWPFRun r : paragrafo.getRuns()) {
            char[] rc = r.text().toCharArray();
            for (char _c : rc) {
                if (_c == c[cont]) {
                    cont++;
                    lr.add(r);
                    if (cont == c.length) {
//                        System.out.println("Encontrado");
                        found = true;
                        break;
                    }
                } else {
                    lr.clear();
                }
            }
            if (found) {
                break;
            }
        }
        return lr;
    }

    public long replaceInParagraphs(Map<String, String> replacements) {
        long count = 0;
        List<XWPFParagraph> xwpfParagraphs = documento.getParagraphs();
        for (XWPFParagraph paragraph : xwpfParagraphs) {
            List<XWPFRun> runs = paragraph.getRuns();

            for (Map.Entry<String, String> replPair : replacements.entrySet()) {
                String find = replPair.getKey();
                String repl = replPair.getValue();

                TextSegment found = paragraph.searchText(find, new PositionInParagraph());
                if (found != null) {
                    count++;
                    if (found.getBeginRun() == found.getEndRun()) {
                        // whole search string is in one Run
                        XWPFRun run = runs.get(found.getBeginRun());
                        String runText = run.getText(run.getTextPosition());
                        String replaced = runText.replace(find, repl);
                        run.setText(replaced, 0);
                    } else {
                        // The search string spans over more than one Run
                        // Put the Strings together
                        StringBuilder b = new StringBuilder();
                        for (int runPos = found.getBeginRun(); runPos <= found.getEndRun(); runPos++) {
                            XWPFRun run = runs.get(runPos);
                            b.append(run.getText(run.getTextPosition()));
                        }
                        String connectedRuns = b.toString();
                        String replaced = connectedRuns.replace(find, repl);

                        // The first Run receives the replaced String of all connected Runs
                        XWPFRun partOne = runs.get(found.getBeginRun());
                        partOne.setText(replaced, 0);
                        // Removing the text in the other Runs.
                        for (int runPos = found.getBeginRun() + 1; runPos <= found.getEndRun(); runPos++) {
                            XWPFRun partNext = runs.get(runPos);
                            partNext.setText("", 0);
                        }
                    }
                }
            }
        }
        return count;
    }
}
